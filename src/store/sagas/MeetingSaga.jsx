import { call, select, put, takeLatest, all } from "redux-saga/effects";
import api from "../../Environment";

import { createNotification } from "react-redux-notify";
import {
  getSuccessNotificationMessage,
  getErrorNotificationMessage,
} from "../../components/helper/NotificationMessage";
import {
  fetchMeetingSuccess,
  fetchMeetingFailure,
  addMeetingSuccess,
  addMeetingFailure,
  deleteMeetingSuccess,
  deleteMeetingFailure,
  searchMeetingSuccess,
  searchMeetingFailure,
  fetchSingleMeetingSuccess,
  fetchSingleMeetingFailure,
} from "../actions/MeetingAction";
import {
  FETCH_MEETING_START,
  ADD_MEETING_START,
  DELETE_MEETING_START,
  SEARCH_MEETING_START,
  FETCH_SINGLE_MEETING_START,
} from "../actions/ActionConstant";

function* fetchMeetingAPI() {
  try {
    const response = yield api.postMethod("instructor/meetings_index");
    if (response.data.success) {
      yield put(fetchMeetingSuccess(response.data));
    } else {
      yield put(fetchMeetingFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(fetchMeetingFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* addMeetingAPI() {
  try {
    const inputData = yield select((state) => state.meeting.addMeeting.data);
    const response = yield api.postMethod(
      "instructor/meetings_save",
      inputData
    );

    if (response.data.success) {
      yield put(addMeetingSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
      window.location.assign(`/single-room/${inputData.room_id}`);
    } else {
      yield put(addMeetingFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(addMeetingFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* deleteMeetingAPI() {
  try {
    const inputData = yield select(
      (state) => state.meeting.deleteMeeting.inputData
    );
    const response = yield api.postMethod(
      "instructor/meetings_delete",
      inputData
    );

    if (response.data.success) {
      yield put(deleteMeetingSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
    } else {
      yield put(deleteMeetingFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(deleteMeetingFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* searchMeetingAPI() {
  try {
    const inputData = yield select(
      (state) => state.meeting.searchMeeting.inputData
    );
    const response = yield api.postMethod(
      "instructor/meetings_search",
      inputData
    );

    if (response.data.success) {
      yield put(searchMeetingSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
    } else {
      yield put(searchMeetingFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(searchMeetingFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* fetchSingleMeetingAPI() {
  try {
    const inputData = yield select(
      (state) => state.meeting.singleMeeting.inputData
    );
    const response = yield api.postMethod(
      "instructor/meetings_view",
      inputData
    );

    if (response.data.success) {
      yield put(fetchSingleMeetingSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
    } else {
      yield put(fetchSingleMeetingFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(fetchSingleMeetingFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

export default function* pageSaga() {
  yield all([yield takeLatest(FETCH_MEETING_START, fetchMeetingAPI)]);
  yield all([yield takeLatest(ADD_MEETING_START, addMeetingAPI)]);
  yield all([yield takeLatest(DELETE_MEETING_START, deleteMeetingAPI)]);
  yield all([yield takeLatest(SEARCH_MEETING_START, searchMeetingAPI)]);
  yield all([
    yield takeLatest(FETCH_SINGLE_MEETING_START, fetchSingleMeetingAPI),
  ]);
}
