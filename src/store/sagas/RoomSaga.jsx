import { call, select, put, takeLatest, all } from "redux-saga/effects";
import api from "../../Environment";

import { createNotification } from "react-redux-notify";
import {
  getSuccessNotificationMessage,
  getErrorNotificationMessage,
} from "../../components/helper/NotificationMessage";
import {
  fetchRoomSuccess,
  fetchRoomFailure,
  addRoomSuccess,
  addRoomFailure,
  deleteRoomSuccess,
  deleteRoomFailure,
  searchRoomSuccess,
  searchRoomFailure,
  fetchSingleRoomSuccess,
  fetchSingleRoomFailure,
  fetchRoomStart,
} from "../actions/RoomAction";
import {
  FETCH_ROOM_START,
  ADD_ROOM_START,
  DELETE_ROOM_START,
  SEARCH_ROOM_START,
  FETCH_SINGLE_ROOM_START,
} from "../actions/ActionConstant";

function* fetchRoomAPI() {
  try {
    const response = yield api.postMethod("instructor/rooms_index");
    if (response.data.success) {
      yield put(fetchRoomSuccess(response.data));
    } else {
      yield put(fetchRoomFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(fetchRoomFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* addRoomAPI() {
  try {
    const inputData = yield select((state) => state.room.addRoom.data);
    const response = yield api.postMethod("instructor/rooms_save", inputData);

    if (response.data.success) {
      yield put(addRoomSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
      window.location.assign("/room-management");
    } else {
      yield put(addRoomFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(addRoomFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* deleteRoomAPI() {
  try {
    const inputData = yield select((state) => state.room.deleteRoom.inputData);
    const response = yield api.postMethod("instructor/rooms_delete", inputData);

    if (response.data.success) {
      yield put(deleteRoomSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
      yield put(fetchRoomStart());
    } else {
      yield put(deleteRoomFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(deleteRoomFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* searchRoomAPI() {
  try {
    const inputData = yield select((state) => state.room.searchRoom.inputData);
    const response = yield api.postMethod("instructor/rooms_search", inputData);

    if (response.data.success) {
      yield put(searchRoomSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
    } else {
      yield put(searchRoomFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(searchRoomFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* fetchSingleRoomAPI() {
  try {
    const inputData = yield select((state) => state.room.singleRoom.inputData);
    const response = yield api.postMethod("instructor/rooms_view", inputData);

    if (response.data.success) {
      yield put(fetchSingleRoomSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
    } else {
      yield put(fetchSingleRoomFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(fetchSingleRoomFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

export default function* pageSaga() {
  yield all([yield takeLatest(FETCH_ROOM_START, fetchRoomAPI)]);
  yield all([yield takeLatest(ADD_ROOM_START, addRoomAPI)]);
  yield all([yield takeLatest(DELETE_ROOM_START, deleteRoomAPI)]);
  yield all([yield takeLatest(SEARCH_ROOM_START, searchRoomAPI)]);
  yield all([yield takeLatest(FETCH_SINGLE_ROOM_START, fetchSingleRoomAPI)]);
}
