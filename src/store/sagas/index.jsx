import { all, fork } from "redux-saga/effects";

import UserSaga from "./UserSaga";
import ChangePasswordSaga from "./ChangePasswordSaga";
import MeetingSaga from "./MeetingSaga";
import SubscriptionSaga from "./SubscriptionSaga";
import CardsSaga from "./CardsSaga";
import InstructorSaga from "./InstructorSaga";
import RoomUserSaga from "./RoomUserSaga";
import RoomSaga from "./RoomSaga";

export default function* rootSaga() {
  yield all([fork(UserSaga)]);
  yield all([fork(ChangePasswordSaga)]);
  yield all([fork(MeetingSaga)]);
  yield all([fork(SubscriptionSaga)]);
  yield all([fork(CardsSaga)]);
  yield all([fork(InstructorSaga)]);
  yield all([fork(RoomSaga)]);
  yield all([fork(RoomUserSaga)]);
}
