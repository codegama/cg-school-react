import { call, select, put, takeLatest, all } from "redux-saga/effects";
import api from "../../Environment";

import { createNotification } from "react-redux-notify";
import {
  getSuccessNotificationMessage,
  getErrorNotificationMessage,
} from "../../components/helper/NotificationMessage";

import {
  FETCH_ROOM_USER_START,
  ADD_ROOM_USER_START,
  DELETE_ROOM_USER_START,
  SEARCH_ROOM_USER_START,
} from "../actions/ActionConstant";

import {
  fetchRoomUserSuccess,
  fetchRoomUserFailure,
  addRoomUserSuccess,
  addRoomUserFailure,
  deleteRoomUserFailure,
  searchRoomUserSuccess,
  deleteRoomUserSuccess,
  searchRoomUserFailure,
} from "../actions/RoomUserAction";

function* fetchRoomUserAPI() {
  try {
    const inputData = yield select(
      (state) => state.roomUser.roomUser.inputData
    );
    const response = yield api.postMethod(
      "instructor/room_users_index",
      inputData
    );
    if (response.data.success) {
      yield put(fetchRoomUserSuccess(response.data));
    } else {
      yield put(fetchRoomUserFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(fetchRoomUserFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* addRoomUserAPI() {
  try {
    const inputData = yield select((state) => state.roomUser.addRoomUser.data);
    const response = yield api.postMethod(
      "instructor/room_users_add",
      inputData
    );

    if (response.data.success) {
      yield put(addRoomUserSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
    } else {
      yield put(addRoomUserFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(addRoomUserFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* deleteRoomUserAPI() {
  try {
    const inputData = yield select(
      (state) => state.roomUser.deleteRoomUser.inputData
    );
    const response = yield api.postMethod(
      "instructor/room_users_remove",
      inputData
    );

    if (response.data.success) {
      yield put(deleteRoomUserSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
    } else {
      yield put(deleteRoomUserFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(deleteRoomUserFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* searchRoomUserAPI() {
  try {
    const inputData = yield select(
      (state) => state.roomUser.searchRoomUser.inputData
    );
    const response = yield api.postMethod(
      "instructor/room_users_search",
      inputData
    );

    if (response.data.success) {
      yield put(searchRoomUserSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
    } else {
      yield put(searchRoomUserFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(searchRoomUserFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

export default function* pageSaga() {
  yield all([yield takeLatest(FETCH_ROOM_USER_START, fetchRoomUserAPI)]);
  yield all([yield takeLatest(ADD_ROOM_USER_START, addRoomUserAPI)]);
  yield all([yield takeLatest(DELETE_ROOM_USER_START, deleteRoomUserAPI)]);
  yield all([yield takeLatest(SEARCH_ROOM_USER_START, searchRoomUserAPI)]);
}
