import { call, select, put, takeLatest, all } from "redux-saga/effects";
import api from "../../Environment";

import { createNotification } from "react-redux-notify";
import {
  getSuccessNotificationMessage,
  getErrorNotificationMessage,
} from "../../components/helper/NotificationMessage";
import {
  fetchInstructorSuccess,
  fetchInstructorFailure,
  addInstructorSuccess,
  addInstructorFailure,
  deleteInstructorSuccess,
  deleteInstructorFailure,
  searchInstructorSuccess,
  searchInstructorFailure,
} from "../actions/InstructorAction";
import {
  FETCH_INSTRUCTOR_START,
  ADD_INSTRUCTOR_START,
  DELETE_INSTRUCTOR_START,
  SEARCH_INSTRUCTOR_START,
} from "../actions/ActionConstant";

function* fetchInstructorAPI() {
  try {
    const response = yield api.postMethod("instructor/sub_instructors_index");
    if (response.data.success) {
      yield put(fetchInstructorSuccess(response.data));
    } else {
      yield put(fetchInstructorFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(fetchInstructorFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* addInstructorAPI() {
  try {
    const inputData = yield select(
      (state) => state.instructor.addInstructor.data
    );
    const response = yield api.postMethod(
      "instructor/sub_instructors_add",
      inputData
    );

    if (response.data.success) {
      yield put(addInstructorSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
    } else {
      yield put(addInstructorFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(addInstructorFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* deleteInstructorAPI() {
  try {
    const inputData = yield select(
      (state) => state.instructor.deleteInstructor.inputData
    );
    const response = yield api.postMethod(
      "instructor/sub_instructors_remove",
      inputData
    );

    if (response.data.success) {
      yield put(deleteInstructorSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
    } else {
      yield put(deleteInstructorFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(deleteInstructorFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

function* searchInstructorAPI() {
  try {
    const inputData = yield select(
      (state) => state.instructor.searchInstructor.inputData
    );
    const response = yield api.postMethod(
      "instructor/sub_instructors_search",
      inputData
    );

    if (response.data.success) {
      yield put(searchInstructorSuccess(response.data));
      const notificationMessage = getSuccessNotificationMessage(
        response.data.message
      );
      yield put(createNotification(notificationMessage));
    } else {
      yield put(searchInstructorFailure(response.data.error));
      const notificationMessage = getErrorNotificationMessage(
        response.data.error
      );
      yield put(createNotification(notificationMessage));
    }
  } catch (error) {
    yield put(searchInstructorFailure(error));
    const notificationMessage = getErrorNotificationMessage(error.message);
    yield put(createNotification(notificationMessage));
  }
}

export default function* pageSaga() {
  yield all([yield takeLatest(FETCH_INSTRUCTOR_START, fetchInstructorAPI)]);
  yield all([yield takeLatest(ADD_INSTRUCTOR_START, addInstructorAPI)]);
  yield all([yield takeLatest(DELETE_INSTRUCTOR_START, deleteInstructorAPI)]);
  yield all([yield takeLatest(SEARCH_INSTRUCTOR_START, searchInstructorAPI)]);
}
