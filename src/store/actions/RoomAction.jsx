import {
  FETCH_ROOM_START,
  FETCH_ROOM_SUCCESS,
  FETCH_ROOM_FAILURE,
  DELETE_ROOM_START,
  DELETE_ROOM_SUCCESS,
  DELETE_ROOM_FAILURE,
  ADD_ROOM_START,
  ADD_ROOM_SUCCESS,
  ADD_ROOM_FAILURE,
  SEARCH_ROOM_START,
  SEARCH_ROOM_SUCCESS,
  SEARCH_ROOM_FAILURE,
  FETCH_SINGLE_ROOM_START,
  FETCH_SINGLE_ROOM_SUCCESS,
  FETCH_SINGLE_ROOM_FAILURE,
} from "./ActionConstant";

// Get Room actions.

export function fetchRoomStart(data) {
  return {
    type: FETCH_ROOM_START,
    data,
  };
}

export function fetchRoomSuccess(data) {
  return {
    type: FETCH_ROOM_SUCCESS,
    data,
  };
}

export function fetchRoomFailure(error) {
  return {
    type: FETCH_ROOM_FAILURE,
    error,
  };
}

// Add Room

export function addRoomStart(data) {
  return {
    type: ADD_ROOM_START,
    data,
  };
}

export function addRoomSuccess(data) {
  return {
    type: ADD_ROOM_SUCCESS,
    data,
  };
}

export function addRoomFailure(error) {
  return {
    type: ADD_ROOM_FAILURE,
    error,
  };
}

// Delete Room actions.

export function deleteRoomStart(data) {
  return {
    type: DELETE_ROOM_START,
    data,
  };
}

export function deleteRoomSuccess(data) {
  return {
    type: DELETE_ROOM_SUCCESS,
    data,
  };
}

export function deleteRoomFailure(error) {
  return {
    type: DELETE_ROOM_FAILURE,
    error,
  };
}

// Search room actions.

export function searchRoomStart(data) {
  return {
    type: SEARCH_ROOM_START,
    data,
  };
}

export function searchRoomSuccess(data) {
  return {
    type: SEARCH_ROOM_SUCCESS,
    data,
  };
}

export function searchRoomFailure(error) {
  return {
    type: SEARCH_ROOM_FAILURE,
    error,
  };
}

// Single room actions.

export function fetchSingleRoomStart(data) {
  return {
    type: FETCH_SINGLE_ROOM_START,
    data,
  };
}

export function fetchSingleRoomSuccess(data) {
  return {
    type: FETCH_SINGLE_ROOM_SUCCESS,
    data,
  };
}

export function fetchSingleRoomFailure(error) {
  return {
    type: FETCH_SINGLE_ROOM_FAILURE,
    error,
  };
}
