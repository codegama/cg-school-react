import {
  FETCH_INSTRUCTOR_START,
  FETCH_INSTRUCTOR_SUCCESS,
  FETCH_INSTRUCTOR_FAILURE,
  DELETE_INSTRUCTOR_START,
  DELETE_INSTRUCTOR_SUCCESS,
  DELETE_INSTRUCTOR_FAILURE,
  ADD_INSTRUCTOR_START,
  ADD_INSTRUCTOR_SUCCESS,
  ADD_INSTRUCTOR_FAILURE,
  SEARCH_INSTRUCTOR_START,
  SEARCH_INSTRUCTOR_SUCCESS,
  SEARCH_INSTRUCTOR_FAILURE,
} from "./ActionConstant";

// Get instructor actions.

export function fetchInstructorStart(data) {
  return {
    type: FETCH_INSTRUCTOR_START,
    data,
  };
}

export function fetchInstructorSuccess(data) {
  return {
    type: FETCH_INSTRUCTOR_SUCCESS,
    data,
  };
}

export function fetchInstructorFailure(error) {
  return {
    type: FETCH_INSTRUCTOR_FAILURE,
    error,
  };
}

// Add instructor

export function addInstructorStart(data) {
  return {
    type: ADD_INSTRUCTOR_START,
    data,
  };
}

export function addInstructorSuccess(data) {
  return {
    type: ADD_INSTRUCTOR_SUCCESS,
    data,
  };
}

export function addInstructorFailure(error) {
  return {
    type: ADD_INSTRUCTOR_FAILURE,
    error,
  };
}

// Delete instructor actions.

export function deleteInstructorStart(data) {
  return {
    type: DELETE_INSTRUCTOR_START,
    data,
  };
}

export function deleteInstructorSuccess(data) {
  return {
    type: DELETE_INSTRUCTOR_SUCCESS,
    data,
  };
}

export function deleteInstructorFailure(error) {
  return {
    type: DELETE_INSTRUCTOR_FAILURE,
    error,
  };
}

// Search instructor actions.

export function searchInstructorStart(data) {
  return {
    type: SEARCH_INSTRUCTOR_START,
    data,
  };
}

export function searchInstructorSuccess(data) {
  return {
    type: SEARCH_INSTRUCTOR_SUCCESS,
    data,
  };
}

export function searchInstructorFailure(error) {
  return {
    type: SEARCH_INSTRUCTOR_FAILURE,
    error,
  };
}
