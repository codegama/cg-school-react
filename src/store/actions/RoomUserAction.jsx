import {
  FETCH_ROOM_USER_START,
  FETCH_ROOM_USER_SUCCESS,
  FETCH_ROOM_USER_FAILURE,
  DELETE_ROOM_USER_START,
  DELETE_ROOM_USER_SUCCESS,
  DELETE_ROOM_USER_FAILURE,
  ADD_ROOM_USER_START,
  ADD_ROOM_USER_SUCCESS,
  ADD_ROOM_USER_FAILURE,
  SEARCH_ROOM_USER_START,
  SEARCH_ROOM_USER_SUCCESS,
  SEARCH_ROOM_USER_FAILURE,
} from "./ActionConstant";

// Get RoomUser actions.

export function fetchRoomUserStart(data) {
  return {
    type: FETCH_ROOM_USER_START,
    data,
  };
}

export function fetchRoomUserSuccess(data) {
  return {
    type: FETCH_ROOM_USER_SUCCESS,
    data,
  };
}

export function fetchRoomUserFailure(error) {
  return {
    type: FETCH_ROOM_USER_FAILURE,
    error,
  };
}

// Add RoomUser

export function addRoomUserStart(data) {
  return {
    type: ADD_ROOM_USER_START,
    data,
  };
}

export function addRoomUserSuccess(data) {
  return {
    type: ADD_ROOM_USER_SUCCESS,
    data,
  };
}

export function addRoomUserFailure(error) {
  return {
    type: ADD_ROOM_USER_FAILURE,
    error,
  };
}

// Delete RoomUser actions.

export function deleteRoomUserStart(data) {
  return {
    type: DELETE_ROOM_USER_START,
    data,
  };
}

export function deleteRoomUserSuccess(data) {
  return {
    type: DELETE_ROOM_USER_SUCCESS,
    data,
  };
}

export function deleteRoomUserFailure(error) {
  return {
    type: DELETE_ROOM_USER_FAILURE,
    error,
  };
}

// Search RoomUser actions.

export function searchRoomUserStart(data) {
  return {
    type: SEARCH_ROOM_USER_START,
    data,
  };
}

export function searchRoomUserSuccess(data) {
  return {
    type: SEARCH_ROOM_USER_SUCCESS,
    data,
  };
}

export function searchRoomUserFailure(error) {
  return {
    type: SEARCH_ROOM_USER_FAILURE,
    error,
  };
}
