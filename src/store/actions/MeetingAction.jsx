import {
  FETCH_MEETING_START,
  FETCH_MEETING_SUCCESS,
  FETCH_MEETING_FAILURE,
  DELETE_MEETING_START,
  DELETE_MEETING_SUCCESS,
  DELETE_MEETING_FAILURE,
  ADD_MEETING_START,
  ADD_MEETING_SUCCESS,
  ADD_MEETING_FAILURE,
  SEARCH_MEETING_START,
  SEARCH_MEETING_SUCCESS,
  SEARCH_MEETING_FAILURE,
  FETCH_SINGLE_MEETING_START,
  FETCH_SINGLE_MEETING_SUCCESS,
  FETCH_SINGLE_MEETING_FAILURE,
} from "./ActionConstant";

// Get Meeting actions.

export function fetchMeetingStart(data) {
  return {
    type: FETCH_MEETING_START,
    data,
  };
}

export function fetchMeetingSuccess(data) {
  return {
    type: FETCH_MEETING_SUCCESS,
    data,
  };
}

export function fetchMeetingFailure(error) {
  return {
    type: FETCH_MEETING_FAILURE,
    error,
  };
}

// Add Meeting

export function addMeetingStart(data) {
  return {
    type: ADD_MEETING_START,
    data,
  };
}

export function addMeetingSuccess(data) {
  return {
    type: ADD_MEETING_SUCCESS,
    data,
  };
}

export function addMeetingFailure(error) {
  return {
    type: ADD_MEETING_FAILURE,
    error,
  };
}

// Delete Meeting actions.

export function deleteMeetingStart(data) {
  return {
    type: DELETE_MEETING_START,
    data,
  };
}

export function deleteMeetingSuccess(data) {
  return {
    type: DELETE_MEETING_SUCCESS,
    data,
  };
}

export function deleteMeetingFailure(error) {
  return {
    type: DELETE_MEETING_FAILURE,
    error,
  };
}

// Search Meeting actions.

export function searchMeetingStart(data) {
  return {
    type: SEARCH_MEETING_START,
    data,
  };
}

export function searchMeetingSuccess(data) {
  return {
    type: SEARCH_MEETING_SUCCESS,
    data,
  };
}

export function searchMeetingFailure(error) {
  return {
    type: SEARCH_MEETING_FAILURE,
    error,
  };
}

// Single Meeting actions.

export function fetchSingleMeetingStart(data) {
  return {
    type: FETCH_SINGLE_MEETING_START,
    data,
  };
}

export function fetchSingleMeetingSuccess(data) {
  return {
    type: FETCH_SINGLE_MEETING_SUCCESS,
    data,
  };
}

export function fetchSingleMeetingFailure(error) {
  return {
    type: FETCH_SINGLE_MEETING_FAILURE,
    error,
  };
}
