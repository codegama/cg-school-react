import {
  FETCH_ROOM_START,
  FETCH_ROOM_SUCCESS,
  FETCH_ROOM_FAILURE,
  ADD_ROOM_START,
  ADD_ROOM_SUCCESS,
  ADD_ROOM_FAILURE,
  DELETE_ROOM_START,
  DELETE_ROOM_SUCCESS,
  DELETE_ROOM_FAILURE,
  SEARCH_ROOM_START,
  SEARCH_ROOM_SUCCESS,
  SEARCH_ROOM_FAILURE,
  FETCH_SINGLE_ROOM_START,
  FETCH_SINGLE_ROOM_SUCCESS,
  FETCH_SINGLE_ROOM_FAILURE,
} from "../actions/ActionConstant";

const initialState = {
  room: {
    data: {},
    loading: true,
    error: false,
  },
  addRoom: {
    data: {},
    loading: true,
    error: false,
  },
  deleteRoom: {
    data: {},
    loading: true,
    error: false,
    inputData: {},
  },
  searchRoom: {
    data: {},
    loading: true,
    error: false,
    inputData: {},
  },
  singleRoom: {
    data: {},
    loading: true,
    error: false,
    inputData: {},
  },
  buttonDisable: false,
  loadingButtonContent: null,
};

const RoomReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ROOM_START:
      return {
        ...state,
        room: {
          data: {},
          loading: true,
          error: false,
        },
      };
    case FETCH_ROOM_SUCCESS:
      return {
        ...state,
        room: {
          data: action.data,
          loading: false,
          error: false,
        },
      };
    case FETCH_ROOM_FAILURE:
      return {
        ...state,
        room: {
          data: {},
          loading: true,
          error: action.error,
        },
      };
    case ADD_ROOM_START:
      return {
        ...state,
        addRoom: {
          data: action.data,
          loading: true,
          error: false,
        },
        buttonDisable: true,
        loadingButtonContent: "Loading.. Please Wait",
      };
    case ADD_ROOM_SUCCESS:
      return {
        ...state,
        addRoom: {
          data: action.data,
          loading: false,
          error: false,
        },
        buttonDisable: false,
        loadingButtonContent: null,
      };
    case ADD_ROOM_FAILURE:
      return {
        ...state,
        addRoom: {
          data: {},
          loading: true,
          error: action.error,
        },
        buttonDisable: false,
        loadingButtonContent: null,
      };
    case DELETE_ROOM_START:
      return {
        ...state,
        deleteRoom: {
          inputData: action.data,
          loading: true,
          error: false,
          data: {},
        },
      };
    case DELETE_ROOM_SUCCESS:
      return {
        ...state,
        deleteRoom: {
          data: action.data,
          loading: false,
          error: false,
          inputData: {},
        },
      };
    case DELETE_ROOM_FAILURE:
      return {
        ...state,
        deleteRoom: {
          data: {},
          loading: true,
          error: action.error,
          inputData: {},
        },
      };
    case SEARCH_ROOM_START:
      return {
        ...state,
        searchRoom: {
          data: {},
          loading: true,
          error: false,
          inputData: action.data,
        },
      };
    case SEARCH_ROOM_SUCCESS:
      return {
        ...state,
        searchRoom: {
          data: action.data,
          loading: false,
          error: false,
          inputData: {},
        },
      };
    case SEARCH_ROOM_FAILURE:
      return {
        ...state,
        searchRoom: {
          data: {},
          loading: true,
          error: action.error,
          inputData: {},
        },
      };
    case FETCH_SINGLE_ROOM_START:
      return {
        ...state,
        singleRoom: {
          data: {},
          loading: true,
          error: false,
          inputData: action.data,
        },
      };
    case FETCH_SINGLE_ROOM_SUCCESS:
      return {
        ...state,
        singleRoom: {
          data: action.data,
          loading: false,
          error: false,
          inputData: {},
        },
      };
    case FETCH_SINGLE_ROOM_FAILURE:
      return {
        ...state,
        singleRoom: {
          data: {},
          loading: true,
          error: action.error,
          inputData: {},
        },
      };

    default:
      return state;
  }
};

export default RoomReducer;
