import {
  FETCH_INSTRUCTOR_START,
  FETCH_INSTRUCTOR_SUCCESS,
  FETCH_INSTRUCTOR_FAILURE,
  ADD_INSTRUCTOR_START,
  ADD_INSTRUCTOR_SUCCESS,
  ADD_INSTRUCTOR_FAILURE,
  DELETE_INSTRUCTOR_START,
  DELETE_INSTRUCTOR_SUCCESS,
  DELETE_INSTRUCTOR_FAILURE,
  SEARCH_INSTRUCTOR_START,
  SEARCH_INSTRUCTOR_SUCCESS,
  SEARCH_INSTRUCTOR_FAILURE,
} from "../actions/ActionConstant";

const initialState = {
  instructor: {
    data: {},
    loading: true,
    error: false,
  },
  addInstructor: {
    data: {},
    loading: true,
    error: false,
  },
  deleteInstructor: {
    data: {},
    loading: true,
    error: false,
    inputData: {},
  },
  searchInstructor: {
    data: {},
    loading: true,
    error: false,
    inputData: {},
  },
  buttonDisable: false,
  loadingButtonContent: null,
};

const InstructorReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_INSTRUCTOR_START:
      return {
        ...state,
        instructor: {
          data: {},
          loading: true,
          error: false,
        },
      };
    case FETCH_INSTRUCTOR_SUCCESS:
      return {
        ...state,
        instructor: {
          data: action.data,
          loading: false,
          error: false,
        },
      };
    case FETCH_INSTRUCTOR_FAILURE:
      return {
        ...state,
        instructor: {
          data: {},
          loading: true,
          error: action.error,
        },
      };
    case ADD_INSTRUCTOR_START:
      return {
        ...state,
        addInstructor: {
          data: action.data,
          loading: true,
          error: false,
        },
      };
    case ADD_INSTRUCTOR_SUCCESS:
      return {
        ...state,
        addInstructor: {
          data: action.data,
          loading: false,
          error: false,
        },
      };
    case ADD_INSTRUCTOR_FAILURE:
      return {
        ...state,
        addInstructor: {
          data: {},
          loading: true,
          error: action.error,
        },
      };
    case DELETE_INSTRUCTOR_START:
      return {
        ...state,
        deleteInstructor: {
          inputData: action.data,
          loading: true,
          error: false,
          data: {},
        },
      };
    case DELETE_INSTRUCTOR_SUCCESS:
      return {
        ...state,
        deleteInstructor: {
          data: action.data,
          loading: false,
          error: false,
          inputData: {},
        },
      };
    case DELETE_INSTRUCTOR_FAILURE:
      return {
        ...state,
        deleteInstructor: {
          data: {},
          loading: true,
          error: action.error,
          inputData: {},
        },
      };
    case SEARCH_INSTRUCTOR_START:
      return {
        ...state,
        searchInstructor: {
          data: {},
          loading: true,
          error: false,
          inputData: action.data,
        },
      };
    case SEARCH_INSTRUCTOR_SUCCESS:
      return {
        ...state,
        searchInstructor: {
          data: action.data,
          loading: false,
          error: false,
          inputData: {},
        },
      };
    case SEARCH_INSTRUCTOR_FAILURE:
      return {
        ...state,
        searchInstructor: {
          data: {},
          loading: true,
          error: action.error,
          inputData: {},
        },
      };

    default:
      return state;
  }
};

export default InstructorReducer;
