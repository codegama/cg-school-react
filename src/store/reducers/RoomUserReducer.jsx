import {
  FETCH_ROOM_USER_START,
  FETCH_ROOM_USER_SUCCESS,
  FETCH_ROOM_USER_FAILURE,
  ADD_ROOM_USER_START,
  ADD_ROOM_USER_SUCCESS,
  ADD_ROOM_USER_FAILURE,
  DELETE_ROOM_USER_START,
  DELETE_ROOM_USER_SUCCESS,
  DELETE_ROOM_USER_FAILURE,
  SEARCH_ROOM_USER_START,
  SEARCH_ROOM_USER_SUCCESS,
  SEARCH_ROOM_USER_FAILURE,
} from "../actions/ActionConstant";

const initialState = {
  roomUser: {
    data: {},
    loading: true,
    error: false,
    inputData: {},
  },
  addRoomUser: {
    data: {},
    loading: true,
    error: false,
  },
  deleteRoomUser: {
    data: {},
    loading: true,
    error: false,
    inputData: {},
  },
  searchRoomUser: {
    data: {},
    loading: true,
    error: false,
    inputData: {},
  },
  buttonDisable: false,
  loadingButtonContent: null,
};

const RoomUserReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ROOM_USER_START:
      return {
        ...state,
        roomUser: {
          data: {},
          loading: true,
          error: false,
          inputData: action.data,
        },
      };
    case FETCH_ROOM_USER_SUCCESS:
      return {
        ...state,
        roomUser: {
          data: action.data,
          loading: false,
          error: false,
          inputData: {},
        },
      };
    case FETCH_ROOM_USER_FAILURE:
      return {
        ...state,
        roomUser: {
          data: {},
          loading: true,
          error: action.error,
          inputData: {},
        },
      };
    case ADD_ROOM_USER_START:
      return {
        ...state,
        addRoomUser: {
          data: action.data,
          loading: true,
          error: false,
        },
      };
    case ADD_ROOM_USER_SUCCESS:
      return {
        ...state,
        addRoomUser: {
          data: action.data,
          loading: false,
          error: false,
        },
      };
    case ADD_ROOM_USER_FAILURE:
      return {
        ...state,
        addRoomUser: {
          data: {},
          loading: true,
          error: action.error,
        },
      };
    case DELETE_ROOM_USER_START:
      return {
        ...state,
        deleteRoomUser: {
          inputData: action.data,
          loading: true,
          error: false,
          data: {},
        },
      };
    case DELETE_ROOM_USER_SUCCESS:
      return {
        ...state,
        deleteRoomUser: {
          data: action.data,
          loading: false,
          error: false,
          inputData: {},
        },
      };
    case DELETE_ROOM_USER_FAILURE:
      return {
        ...state,
        deleteRoomUser: {
          data: {},
          loading: true,
          error: action.error,
          inputData: {},
        },
      };
    case SEARCH_ROOM_USER_START:
      return {
        ...state,
        searchRoomUser: {
          data: {},
          loading: true,
          error: false,
          inputData: action.data,
        },
      };
    case SEARCH_ROOM_USER_SUCCESS:
      return {
        ...state,
        searchRoomUser: {
          data: action.data,
          loading: false,
          error: false,
          inputData: {},
        },
      };
    case SEARCH_ROOM_USER_FAILURE:
      return {
        ...state,
        searchRoomUser: {
          data: {},
          loading: true,
          error: action.error,
          inputData: {},
        },
      };

    default:
      return state;
  }
};

export default RoomUserReducer;
