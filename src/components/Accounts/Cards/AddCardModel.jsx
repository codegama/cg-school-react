import React, { useState } from "react";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import api from "../../../Environment";
import { connect } from "react-redux";
import { createNotification } from "react-redux-notify";
import {
  getSuccessNotificationMessage,
  getErrorNotificationMessage,
} from "../../helper/NotificationMessage";
import { fetchCardDetailsStart } from "../../../store/actions/CardsAction";

const AddCardModel = ({ dispatch }) => {
  const stripe = useStripe();
  const elements = useElements();
  const [addCardButtonDisable, setAddCardButtonDisable] = useState(false);
  const [addCardLoadingContent, setAddCardLoadingContent] = useState(null);

  const addCard = async (ev) => {
    ev.preventDefault();
    setAddCardButtonDisable(true);
    setAddCardLoadingContent("Loading... Please wait");
    if (stripe) {
      await stripe
        .createPaymentMethod({
          type: "card",
          card: elements.getElement(CardElement),
        })
        .then((payload) => {
          const inputData = {
            card_token: payload.paymentMethod.id,
          };
          api
            .postMethod("instructor/cards_add", inputData)
            .then((response) => {
              if (response.data.success) {
                const notificationMessage = getSuccessNotificationMessage(
                  response.data.message
                );
                dispatch(createNotification(notificationMessage));
                dispatch(fetchCardDetailsStart());

                setAddCardButtonDisable(false);
                setAddCardLoadingContent(null);
                this.props.cardAddedStatusChange();
              } else {
                const notificationMessage = getErrorNotificationMessage(
                  response.data.error
                );
                dispatch(createNotification(notificationMessage));
              }
            })
            .catch((error) => {
              setAddCardButtonDisable(false);
              setAddCardLoadingContent(null);
              const notificationMessage = getErrorNotificationMessage(
                "Error Please try again"
              );
              dispatch(createNotification(notificationMessage));
            });
        })
        .catch((error) => {
          console.log("Eroor", error);
          setAddCardButtonDisable(false);
          setAddCardLoadingContent(null);
          const notificationMessage = getErrorNotificationMessage(
            "Please check your card details and try again.."
          );
          dispatch(createNotification(notificationMessage));
        });
    } else {
      setAddCardButtonDisable(false);
      setAddCardLoadingContent(null);
      const notificationMessage = getErrorNotificationMessage(
        "Stripe is not configured"
      );
      dispatch(createNotification(notificationMessage));
    }
  };

  return (
    <div className="ul-widget__payment-form">
      <form onSubmit={addCard}>
        <div className="form-row">
          <CardElement />
        </div>
        <div className="row">
          <div className="col-md-2">
            <button
              className="btn btn-primary btn-block m-1"
              type="button"
              disabled={addCardButtonDisable}
              onClick={addCard}
            >
              {addCardLoadingContent != null ? addCardLoadingContent : "Add"}
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(null, mapDispatchToProps)(AddCardModel);
