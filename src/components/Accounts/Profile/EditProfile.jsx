import React, { Component } from "react";
import { connect } from "react-redux";
import {
  fetchUserDetailsStart,
  editUserDetails,
  updateUserDetailsStart,
} from "../../../store/actions/UserAction";

class EditProfile extends Component {
  state = {
    loading: true,
    inputData: {},
    imagePreviewUrl: null,
  };
  componentDidMount() {
    if (this.props.userData.profile.loading)
      this.props.dispatch(fetchUserDetailsStart());
  }
  handleChange = ({ currentTarget: input }) => {
    this.props.dispatch(editUserDetails(input.name, input.value));
  };
  handleChangeImage = ({ currentTarget: input }) => {
    const inputData = { ...this.state.inputData };
    if (input.type === "file") {
      inputData[input.name] = input.files[0];
      this.setState({ inputData });
    }
    let reader = new FileReader();
    let file = input.files[0];

    reader.onloadend = () => {
      if (input.name == "picture")
        this.setState({
          imagePreviewUrl: reader.result,
        });
    };
    if (file) {
      reader.readAsDataURL(file);
      setTimeout(() => {
        this.props.dispatch(updateUserDetailsStart(input.files[0]));
      }, 1000);
    }
  };
  updateProfile = (event) => {
    event.preventDefault();
    this.props.dispatch(updateUserDetailsStart());
  };
  render() {
    const { profile } = this.props.userData;
    if (profile.loading === true) {
      return "Loading...";
    } else {
      return (
        <div className="card mb-4">
          <div className="card-header bg-transparent">
            <h4>Edit Profile</h4>
          </div>
          <div className="card-body">
            <div className="row">
              <div className="col-md-6">
                <form onSubmit={this.updateProfile}>
                  <div className="row">
                    <div className="col-md-12 form-group mb-3">
                      <label for="Name">Name</label>

                      <input
                        className="form-control"
                        id="firstName1"
                        type="text"
                        placeholder="Enter your name"
                        name="name"
                        value={profile.data.instructor_details.name}
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="col-md-12 form-group mb-3">
                      <label for="exampleInputEmail1">Email address</label>
                      <input
                        className="form-control"
                        id="exampleInputEmail1"
                        type="email"
                        placeholder="Enter email"
                        name="email"
                        value={profile.data.instructor_details.email}
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="col-md-12 form-group mb-3">
                      <label for="phone">Phone</label>
                      <input
                        className="form-control"
                        id="phone"
                        placeholder="Enter phone"
                        name="mobile"
                        value={profile.data.instructor_details.mobile}
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="col-md-12 form-group mb-3">
                      <label for="website">About Me</label>
                      <textarea
                        className="form-control"
                        id="website"
                        placeholder="Web address"
                        name="description"
                        value={profile.data.instructor_details.description}
                        onChange={this.handleChange}
                      ></textarea>
                    </div>
                    <div className="col-md-12">
                      <div className="upload-btn-wrapper">
                        <button className="upload-btn">Upload a image</button>
                        <input
                          type="file"
                          name="picture"
                          onChange={this.handleChangeImage}
                        />
                      </div>
                    </div>
                    <div className="col-md-12 align-center">
                      <button
                        className="btn btn-primary"
                        type="submit"
                        disabled={this.props.userData.buttonDisable}
                      >
                        {" "}
                        {this.props.userData.loadingButtonContent != null
                          ? this.props.userData.loadingButtonContent
                          : "Submit"}
                      </button>
                    </div>
                  </div>
                </form>
              </div>
              <div className="col-md-6">
                <img
                  src={
                    window.location.origin + "/assets/images/edit-profile.svg"
                  }
                  className="img-fluid create-meeting-img"
                />
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToPros = (state) => ({
  userData: state.users,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(mapStateToPros, mapDispatchToProps)(EditProfile);
