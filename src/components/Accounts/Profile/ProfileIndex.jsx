import React, { Component } from "react";
import { connect } from "react-redux";
import ProfileLoader from "../../Loader/ProfileLoader";
import {
  fetchUserDetailsStart,
  editUserDetails,
  updateUserDetailsStart,
} from "../../../store/actions/UserAction";
import { Link } from "react-router-dom";

class ProfileIndex extends Component {
  state = {
    displayAccountAccountModel: false,
  };
  componentDidMount() {
    if (this.props.userData.profile.loading)
      this.props.dispatch(fetchUserDetailsStart());
  }

  render() {
    const userData = this.props.userData.profile;
    if (userData.loading == true) {
      return (
        <div className="main-wrapper profile xs-padding">
          <div className="container">
            <div className="row">
              <ProfileLoader />
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="card user-profile o-hidden mb-4">
          <div
            className="header-cover"
            style={{
              backgroundImage: `url(
                ${window.location.origin}/assets/images/photo-wide-4.jpg
              )`,
            }}
          ></div>
          <div className="user-content">
            <div className="user-info">
              <img
                className="profile-picture avatar-lg mb-2"
                src={window.location.origin + "/assets/images/faces/1.jpg"}
                alt=""
              />
              <p className="m-0 text-24">Beno Darry</p>
              <p className="text-muted m-0">User</p>
            </div>
            <div class="edit-btn">
              <a href="#">
                <button class="btn btn-outline-primary width-200 btn-md m-1" type="button">Edit Profile</button>
              </a>
            </div>
          </div>
          <div className="card-body align-center">
            <div className="row">
              <div className="col-md-12">
                <div className="row">
                  <div className="col-md-4">
                    <div className="mb-4">
                      <p className="text-primary mb-1">
                        <i className="i-Calendar text-16 mr-1"></i> Birth Date
                      </p>
                      <span>1 Jan, 1994</span>
                    </div>
                    <div className="mb-4">
                      <p className="text-primary mb-1">
                        <i className="i-MaleFemale text-16 mr-1"></i> Email
                      </p>
                      <span>beno@codegama.com</span>
                    </div>
                    <div className="mb-4">
                      <p className="text-primary mb-1">
                        <i className="i-Globe text-16 mr-1"></i> Lives In
                      </p>
                      <span>Banglore</span>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <div className="mb-4">
                      <p className="text-primary mb-1">
                        <i className="i-MaleFemale text-16 mr-1"></i> Gender
                      </p>
                      <span>Male</span>
                    </div>
                    <div className="mb-4">
                      <p className="text-primary mb-1">
                        <i className="i-MaleFemale text-16 mr-1"></i> Phone
                        Number
                      </p>
                      <span>+91 7708811015</span>
                    </div>
                    <div className="mb-4">
                      <p className="text-primary mb-1">
                        <i className="i-Cloud-Weather text-16 mr-1"></i> About
                        Me
                      </p>
                      <span>
                        Lorem ipsum, dolor sit amet consectetur adipisicing
                        elit. Libero quis beatae officia saepe perferendis
                        voluptatum minima eveniet voluptates dolorum, temporibus
                        nisi maxime nesciunt totam repudiandae commodi sequi
                        dolor quibusdam
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToPros = (state) => ({
  userData: state.users,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(mapStateToPros, mapDispatchToProps)(ProfileIndex);
