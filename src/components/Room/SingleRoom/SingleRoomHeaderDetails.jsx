import React from "react";
import { connect } from "react-redux";

const SingleRoomHeaderDetails = (props) => {
  const singleRoom = props.singleRoom;
  return (
    <div className="row mt-4">
      <div className="col-xl-12 col-md-12 mb-4">
        <div className="card mb-4 single-room-meeting">
          <div className="card-header bg-transparent">
            <h4>Single Room Meeting</h4>
          </div>
          {singleRoom.loading ? (
            "Loading..."
          ) : (
            <div className="card-body">
              <div className="ul-widget5">
                <div className="ul-widget5__item">
                  <div className="ul-widget5__content resp-display-block">
                    <div className="ul-widget5__pic single-meeting-pic">
                      <img
                        src={singleRoom.data.data.room_details.picture}
                        alt="Third slide"
                        className="single-meeting-img"
                      />
                    </div>
                    <div className="ul-widget5__section resp-padding-top resp-padding-top-md">
                      <a className="ul-widget4__title" href="#">
                        {singleRoom.data.data.room_details.title}
                      </a>
                      <p className="ul-widget5__desc">
                        {singleRoom.data.data.room_details.description}
                      </p>
                      <div className="ul-widget5__info">
                        <span>Date</span>
                        <span className="text-primary">26 July 2020</span>
                        <span>Time</span>
                        <span className="text-primary">8:45 PM</span>
                      </div>
                    </div>
                  </div>
                  <div className="ul-widget5__content">
                    <div className="ul-widget5__section">
                      <a className="ul-widget4__title" href="#">
                        <span>No of Classes</span>
                      </a>
                      <p className="ul-widget5__desc">20 classes</p>
                    </div>
                  </div>
                  <div className="ul-widget5__content">
                    <div className="ul-widget5__section">
                      <a className="ul-widget4__title" href="#">
                        <span>No of Users</span>
                      </a>
                      <p className="ul-widget5__desc">20 Users</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

const mapStateToPros = (state) => ({
  singleRoom: state.room.singleRoom,
});

export default connect(mapStateToPros, null)(SingleRoomHeaderDetails);
