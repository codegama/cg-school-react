import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { deleteMeetingStart } from "../../../store/actions/MeetingAction";

const SingleRoomMeetingList = (props) => {
  const meeting = props.meeting;
  const singleRoom = props.singleRoom;

  const deleteMeeting = (event, meeting) => {
    event.preventDefault();
    this.props.dispatch(deleteMeetingStart({ meeting_id: meeting.id }));
  };
  return (
    <div className="row">
      <div className="col-md-12 col-xl-12 mb-4">
        <div className="card mb-4">
          <div className="card-header flex-content">
            <h4>Meeting List</h4>
            <a href="create-room.html">
              {singleRoom.loading ? (
                "Loading"
              ) : (
                <Link
                  className="btn btn-primary width-200 btn-lg m-1"
                  to={`/create-meeting/${singleRoom.data.data.room_details.room_id}`}
                >
                  <i className="fas fa-plus mr-2"></i>Create Meeting
                </Link>
              )}
            </a>
          </div>
          <div className="card-body">
            {meeting.loading
              ? "Loading"
              : meeting.data.data.meetings.length > 0
              ? meeting.data.data.meetings.map((data) => (
                  <div className="ul-widget__body">
                    <div className="ul-widget5">
                      <div className="ul-widget5__item section-display">
                        <div className="ul-widget5__content">
                          <div className="ul-widget5__pic">
                            <img src={data.picture} alt="Third slide" />
                          </div>
                          <div className="ul-widget5__section resp-padding-top">
                            <Link
                              className="ul-widget4__title"
                              to={`/single-meeting/${data.id}`}
                            >
                              {data.title}
                            </Link>
                            <p className="ul-widget5__desc">
                              {data.description}
                            </p>
                            <div className="ul-widget5__info">
                              <span>Instructor:</span>
                              <span className="text-primary"> You</span>
                              <span>Created:</span>
                              <span className="text-primary">26 July 2020</span>
                            </div>
                          </div>
                        </div>
                        <div className="ul-widget5__content">
                          <div className="ul-widget5__section">
                            <a className="ul-widget4__title" href="#">
                              <span>No of Classes</span>
                            </a>
                            <p className="ul-widget5__desc">20 classes</p>
                          </div>
                        </div>
                        <div className="ul-widget5__content">
                          <div className="ul-widget5__section">
                            <a className="ul-widget4__title" href="#">
                              <span>No of Users</span>
                            </a>
                            <p className="ul-widget5__desc">20 Users</p>
                          </div>
                        </div>
                        <div className="ul-widget5__content">
                          <button
                            className="btn bg-white _r_btn border-0"
                            type="button"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          >
                            <p className="meeting-dot bg-primary"></p>
                            <p className="meeting-dot bg-primary"></p>
                            <p className="meeting-dot bg-primary"></p>
                          </button>
                          <div
                            className="dropdown-menu"
                            x-placement="bottom-start"
                            style={{
                              position: "absolute",
                              transform: "translate3d(0px, 33px, 0px)",
                              top: "0px",
                              left: "0px",
                              willChange: "transform",
                            }}
                          >
                            <Link className="dropdown-item" to={"#"}>
                              Edit
                            </Link>
                            <a
                              className="dropdown-item"
                              href="#"
                              onClick={(event) => deleteMeeting(event, data)}
                            >
                              Delete
                            </a>
                            <Link
                              className="dropdown-item"
                              to={`/single-meeting/${data.id}`}
                            >
                              View
                            </Link>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                ))
              : "No data Found"}
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToPros = (state) => ({
  meeting: state.meeting.meeting,
  singleRoom: state.room.singleRoom,
});

export default connect(mapStateToPros, null)(SingleRoomMeetingList);
