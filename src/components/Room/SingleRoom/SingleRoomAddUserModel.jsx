import React from "react";

const SingleRoomAddUserModel = () => {
  return (
    <div
      className="modal fade"
      id="exampleModalCenter"
      tabindex="-1"
      role="dialog"
      aria-labelledby="exampleModalCenterTitle-2"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalCenterTitle-2">
              Add Instructor
            </h5>
            <button
              className="close"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div className="modal-body">
            <form>
              <div className="row">
                <div className="form-group col-md-6 mb-3">
                  <label for="firstName1">First name</label>
                  <input
                    className="form-control"
                    id="firstName1"
                    type="text"
                    placeholder="Enter your first name"
                  />
                </div>
                <div className="form-group col-md-6 mb-3">
                  <label for="lastName1">Last name</label>
                  <input
                    className="form-control"
                    id="lastName1"
                    type="text"
                    placeholder="Enter your last name"
                  />
                </div>
              </div>
              <div className="form-group mb-3">
                <label for="exampleInputEmail1">Email address</label>
                <input
                  className="form-control"
                  id="exampleInputEmail1"
                  type="email"
                  placeholder="Enter email"
                />
                <small id="emailHelp" className="form-text text-muted">
                  We'll never share your email with anyone else.
                </small>
              </div>
              <div className="form-group mb-3">
                <label for="phone">Phone(Optional)</label>
                <input
                  className="form-control"
                  id="phone"
                  placeholder="Enter phone"
                />
              </div>
              <div className="form-group mb-3">
                <label for="Password">Password</label>
                <input
                  className="form-control"
                  type="password"
                  id="Password"
                  placeholder="Enter Password"
                />
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button
              className="btn btn-secondary"
              type="button"
              data-dismiss="modal"
            >
              Close
            </button>
            <button className="btn btn-primary ml-2" type="button">
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SingleRoomAddUserModel;
