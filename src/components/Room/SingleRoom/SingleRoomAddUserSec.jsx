import React, { useState } from "react";
import { connect } from "react-redux";
import { searchRoomUserStart } from "../../../store/actions/RoomUserAction";

const SingleRoomAddUserSec = (props) => {
  const [searchKey, setSearchkey] = useState("");

  const handleChange = ({ currentTarget: input }) => {
    setSearchkey(input.value);
    if (searchKey.length > 1)
      props.dispatch(searchRoomUserStart({ search_key: input.value }));
  };

  return (
    <div className="row">
      <div className="col-xl-12 col-md-12 mb-4">
        <div className="card mb-4">
          <div className="card-header d-flex align-items-center bg-transparent resp-justify-center">
            <h3 className="w-50 float-left card-title m-0">Add User</h3>
          </div>
          <div className="card-body">
            <form>
              <div className="form-group row mb-4">
                <label className="col-sm-2 col-form-label" for="date-time">
                  Add user
                </label>
                <div className="col-sm-5">
                  <input
                    className="form-control"
                    id="date-time"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                    type="text"
                    name="search_key"
                    value={searchKey}
                    onChange={handleChange}
                  />
                  <div
                    className="dropdown-menu instructor-dropdown-1"
                    x-placement="bottom-start"
                    style={{
                      position: "absolute",
                      transform: "translate3d(0px, 33px, 0px)",
                      top: "0px",
                      left: "0px",
                      willChange: "transform",
                    }}
                  >
                    <div className="ul-widget3-header mb-3">
                      <div className="ul-widget3-img">
                        <img
                          id="userDropdown"
                          src={
                            window.location.origin +
                            "/assets/images/faces/1.jpg"
                          }
                        />
                      </div>
                      <div className="ul-widget3-info">
                        <a className="__g-widget-username" href="#">
                          <span className="t-font-bolder">
                            Timithy Clarkson
                          </span>
                        </a>
                      </div>
                      <div className="add-btn ml-auto">
                        <button
                          className="btn btn-primary btn-sm m-1"
                          type="button"
                        >
                          Add
                        </button>
                      </div>
                    </div>
                    <div className="ul-widget3-header">
                      <div className="ul-widget3-img">
                        <img
                          id="userDropdown"
                          src={
                            window.location.origin +
                            "/assets/images/faces/1.jpg"
                          }
                        />
                      </div>
                      <div className="ul-widget3-info">
                        <a className="__g-widget-username" href="#">
                          <span className="t-font-bolder">Beno darry</span>
                        </a>
                      </div>
                      <div className="add-btn ml-auto">
                        <button
                          className="btn btn-primary btn-sm m-1"
                          type="button"
                        >
                          Add
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-sm-5 text-center">
                  <button
                    className="btn btn-primary btn-lg add-i"
                    type="button"
                    data-toggle="modal"
                    data-target="#exampleModalCenter"
                  >
                    <i className="fas fa-plus mr-2"></i>Add User
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToPros = (state) => ({
  roomUser: state.roomUser,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(
  mapStateToPros,
  mapDispatchToProps
)(SingleRoomAddUserSec);
