import React from "react";
import { connect } from "react-redux";

const SingleRoomUserTableSec = (props) => {
  const { roomUser } = props.roomUser;
  return (
    <div className="row">
      <div className="col-xl-12 col-md-12">
        {roomUser.loading ? (
          "Loading..."
        ) : roomUser.data.data.room_users.length > 0 ? (
          <div className="card o-hidden mb-4">
            <div className="card-header d-flex align-items-center border-0">
              <h3 className="w-50 float-left card-title m-0">User List</h3>
              <div className="search-bar">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Search"
                />
                <i className="search-icon text-muted i-Magnifi-Glass1"></i>
              </div>
            </div>
            <div>
              <div className="table-responsive">
                <table className="table text-center" id="user_table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">User Name</th>
                      <th scope="col">Address</th>
                      <th scope="col">Date Of Joining</th>
                      <th scope="col">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {roomUser.data.data.room_users.map((user, i) => (
                      <tr>
                        <th scope="row">1</th>
                        <td>
                          <a href="">
                            <div className="ul-widget-app__profile-pic">
                              <img
                                className="profile-picture avatar-sm mr-2 mb-2 rounded-circle img-fluid"
                                src={
                                  window.location.origin +
                                  "/assets/images/faces/1.jpg"
                                }
                                alt=""
                              />{" "}
                              Jhon Wick
                            </div>
                          </a>
                        </td>
                        <td>Banglore</td>
                        <td>18-07-2020</td>
                        <td>
                          <a className="text-success mr-2" href="#">
                            <i className="nav-icon i-Pen-2 font-weight-bold"></i>
                          </a>
                          <a className="text-danger mr-2" href="#">
                            <i className="nav-icon i-Close-Window font-weight-bold"></i>
                          </a>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        ) : (
          "No data Found"
        )}
      </div>
    </div>
  );
};

const mapStateToPros = (state) => ({
  roomUser: state.roomUser,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(
  mapStateToPros,
  mapDispatchToProps
)(SingleRoomUserTableSec);
