import React, { Component } from "react";
import SingleRoomMeetingList from "./SingleRoomMeetingList";
import SingleRoomAddUserSec from "./SingleRoomAddUserSec";
import SingleRoomUserTableSec from "./SingleRoomUserTableSec";
import SingleRoomHeaderDetails from "./SingleRoomHeaderDetails";
import SingleRoomAddUserModel from "./SingleRoomAddUserModel";
import { connect } from "react-redux";
import { fetchSingleRoomStart } from "../../../store/actions/RoomAction";
import { fetchMeetingStart } from "../../../store/actions/MeetingAction";
import { fetchRoomUserStart } from "../../../store/actions/RoomUserAction";

class SingleRoomIndex extends Component {
  state = {};
  componentDidMount() {
    this.props.dispatch(
      fetchSingleRoomStart({ room_id: this.props.match.params.id })
    );
    this.props.dispatch(fetchMeetingStart());
    this.props.dispatch(
      fetchRoomUserStart({ room_id: this.props.match.params.id })
    );
  }

  render() {
    return (
      <section className="widgets-content">
        <SingleRoomHeaderDetails />
        <SingleRoomMeetingList />
        <SingleRoomAddUserSec />
        <SingleRoomUserTableSec />
        <SingleRoomAddUserModel />
      </section>
    );
  }
}

const mapStateToPros = (state) => ({
  room: state.room,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(mapStateToPros, mapDispatchToProps)(SingleRoomIndex);
