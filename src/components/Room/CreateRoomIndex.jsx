import React, { Component } from "react";
import { connect } from "react-redux";
import { addRoomStart } from "../../store/actions/RoomAction";

class CreateRoomIndex extends Component {
  state = {
    inputData: {},
  };

  handleChange = ({ currentTarget: input }) => {
    const inputData = { ...this.state.inputData };
    inputData[input.name] = input.value;
    this.setState({ inputData });
  };
  handleSubmit = (event) => {
    event.preventDefault();
    this.props.dispatch(addRoomStart(this.state.inputData));
  };

  handleChangeImage = ({ currentTarget: input }) => {
    const inputData = { ...this.state.inputData };
    if (input.type === "file") {
      inputData[input.name] = input.files[0];
      this.setState({ inputData });
    }
    let reader = new FileReader();
    let file = input.files[0];

    reader.onloadend = () => {
      if (input.name == "picture")
        this.setState({
          imagePreviewUrl: reader.result,
        });
    };
    if (file) {
      reader.readAsDataURL(file);
    }
  };

  render() {
    const { inputData } = this.state;
    return (
      <section class="widgets-content">
        <div class="row mt-4">
          <div class="col-xl-12 col-md-12 mb-4">
            <div class="card">
              <div class="card-header bg-transparent">
                <h4>Create Room</h4>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-md-6">
                    <form onSubmit={this.handleSubmit}>
                      <div class="form-group row">
                        <label
                          class="col-sm-3 col-form-label"
                          for="inputEmail3"
                        >
                          Title
                        </label>
                        <div class="col-sm-9">
                          <input
                            class="form-control"
                            id="inputEmail3"
                            type="text"
                            value={inputData.title}
                            name="title"
                            onChange={this.handleChange}
                          />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="desc">
                          Description
                        </label>
                        <div class="col-sm-9">
                          <textarea
                            class="form-control"
                            id="desc"
                            type="text"
                            value={inputData.description}
                            name="description"
                            onChange={this.handleChange}
                          ></textarea>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label
                          class="col-sm-3 col-form-label"
                          for="inputEmail3"
                        >
                          Upload Image
                        </label>
                        <div class="col-sm-9">
                          <div class="upload-btn-wrapper create-room">
                            <button class="upload-btn">Upload a image</button>
                            <input
                              type="file"
                              name="picture"
                              onChange={this.handleChangeImage}
                            />
                          </div>
                        </div>
                      </div>

                      <div class="form-group row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                          <button
                            class="btn btn-block btn-primary"
                            type="submit"
                            disabled={this.props.room.buttonDisable}
                          >
                            {this.props.room.loadingButtonContent != null
                              ? this.props.room.loadingButtonContent
                              : "Create Room"}
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="col-md-6">
                    <img
                      src={
                        window.location.origin +
                        "/assets/images/create-room.svg"
                      }
                      class="img-fluid create-room-img"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToPros = (state) => ({
  room: state.room,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(mapStateToPros, mapDispatchToProps)(CreateRoomIndex);
