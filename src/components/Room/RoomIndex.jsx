import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  fetchRoomStart,
  deleteRoomStart,
} from "../../store/actions/RoomAction";

class RoomIndex extends Component {
  state = {};

  componentDidMount() {
    if (this.props.room.room.loading) this.props.dispatch(fetchRoomStart());
  }

  deleteRoom = (event, room) => {
    event.preventDefault();
    this.props.dispatch(deleteRoomStart({ room_id: room.room_id }));
  };

  render() {
    const { room } = this.props.room;
    return (
      <section class="widgets-content">
        <div class="row mt-4">
          <div class="col-xl-12 col-md-12 mb-4">
            <div class="card">
              <div class="card-header flex-content bg-transparent">
                <h4>Room Management</h4>
                <a href="create-room.html">
                  <Link
                    class="btn btn-primary width-200 btn-lg m-1"
                    to={"/create-room"}
                  >
                    <i class="fas fa-plus mr-2"></i>Create Room
                  </Link>
                </a>
              </div>
              <div class="card-body">
                {room.loading
                  ? "Loading..."
                  : room.data.data.rooms.length > 0
                  ? room.data.data.rooms.map((room) => (
                      <div class="ul-widget__body">
                        <div class="ul-widget5">
                          <div class="ul-widget5__item section-display">
                            <div class="ul-widget5__content">
                              <div class="ul-widget5__pic">
                                <img src={room.picture} alt="Third slide" />
                              </div>
                              <div class="ul-widget5__section resp-padding-top ">
                                <Link
                                  class="ul-widget4__title"
                                  to={`/single-room/${room.unique_id}`}
                                >
                                  {room.title}
                                </Link>
                                <p class="ul-widget5__desc">
                                  {room.description}
                                </p>
                                <div class="ul-widget5__info">
                                  <span>Instructor:</span>
                                  <span class="text-primary"> You</span>
                                  <span>Created:</span>
                                  <span class="text-primary">26 July 2020</span>
                                </div>
                              </div>
                            </div>
                            <div class="ul-widget5__content">
                              <div class="ul-widget5__section">
                                <a class="ul-widget4__title" href="#">
                                  <span>No of Classes</span>
                                </a>
                                <p class="ul-widget5__desc">20 classes</p>
                              </div>
                            </div>
                            <div class="ul-widget5__content">
                              <div class="ul-widget5__section">
                                <a class="ul-widget4__title" href="#">
                                  <span>No of Users</span>
                                </a>
                                <p class="ul-widget5__desc">20 Users</p>
                              </div>
                            </div>
                            <div class="ul-widget5__content">
                              <button
                                class="btn bg-white _r_btn border-0"
                                type="button"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                              >
                                <p class="meeting-dot bg-primary"></p>
                                <p class="meeting-dot bg-primary"></p>
                                <p class="meeting-dot bg-primary"></p>
                              </button>
                              <div
                                class="dropdown-menu"
                                x-placement="bottom-start"
                                style={{
                                  position: "absolute",
                                  transform: "translate3d(0px, 33px, 0px)",
                                  top: "0px",
                                  left: "0px",
                                  willChange: "transform",
                                }}
                              >
                                <Link class="dropdown-item" to="#">
                                  Edit
                                </Link>
                                <a
                                  class="dropdown-item"
                                  href="#"
                                  onClick={(event) =>
                                    this.deleteRoom(event, room)
                                  }
                                >
                                  Delete
                                </a>
                                <Link
                                  class="dropdown-item"
                                  to={`/single-room/${room.unique_id}`}
                                >
                                  View
                                </Link>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    ))
                  : "No Data Found"}
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToPros = (state) => ({
  room: state.room,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(mapStateToPros, mapDispatchToProps)(RoomIndex);
