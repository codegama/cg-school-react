import React from "react";
import { Link } from "react-router-dom";

const AuthHeader = () => {
  return (
    <header class="header landing-sec">
      <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container-fluid">
          <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarTogglerDemo01"
            aria-controls="navbarTogglerDemo01"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-icon"></span>
            <span class="navbar-icon"></span>
            <span class="navbar-icon"></span>
          </button>
          <a class="navbar-brand" href="index.html">
            <img
              src={window.location.origin + "/assets/images/logo.png"}
              class="logo"
            />
          </a>
          <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
            <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
              <li class="nav-item active">
                <Link class="nav-link smooth" to={"/login"}>
                  Login
                </Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link smooth" to={"/register"}>
                  Register
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <section class="sm-padding">
        <div class="container">
          <div class="row">
            <div class="col-md-5 col-sm-12 col-xs-12">
              <div class="banner-info">
                <h3 class="title">
                  Zoom Clone, A Video Conferencing Solution Built For Success
                </h3>
                <p class="desc">
                  Pikchat’s Zoom clone script is an innovative approach to the
                  primitive concept of video conferencing, coupled with the most
                  advanced business communication tools the world has to offer.
                  A tool for the modern business.
                </p>
                <button class="btn btn-primary width-200">Contact Sales</button>
              </div>
            </div>
            <div class="col-md-7 col-sm-12 col-xs-12">
              <img
                src={
                  window.location.origin +
                  "/assets/images/landing-page/banner-img.png"
                }
                class="img-fluid"
              />
            </div>
          </div>
        </div>
      </section>
    </header>
  );
};

export default AuthHeader;
