import React, { Component } from "react";

class AuthFooter extends Component {
  state = {};
  render() {
    return (
      <>
        <section className="sm-padding footer-sec">
          <div className="container">
            <div className="row">
              <div className="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <h5 className="title-head">Industries</h5>
                <ul className="list-unstyled">
                  <li className="list-link">
                    <a href="#">On-demand</a>
                  </li>
                  <li className="list-link">
                    <a href="#">Online community</a>
                  </li>
                  <li className="list-link">
                    <a href="#">Market place</a>
                  </li>
                  <li className="list-link">
                    <a href="#">Gaming</a>
                  </li>
                  <li className="list-link">
                    <a href="#">Live streaming</a>
                  </li>
                </ul>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <h5 className="title-head">Features</h5>
                <ul className="list-unstyled">
                  <li className="list-link">
                    <a href="#">In app chat</a>
                  </li>
                  <li className="list-link">
                    <a href="#">Live chat</a>
                  </li>
                  <li className="list-link">
                    <a href="#">Online community</a>
                  </li>
                  <li className="list-link">
                    <a href="#">Chat encryption</a>
                  </li>
                </ul>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <h5 className="title-head">Learn & Connect</h5>
                <ul className="list-unstyled">
                  <li className="list-link">
                    <a href="#">About US</a>
                  </li>
                  <li className="list-link">
                    <a href="#">Contact</a>
                  </li>
                  <li className="list-link">
                    <a href="#">Help Docs</a>
                  </li>
                  <li className="list-link">
                    <a href="#">Privacy </a>
                  </li>
                  <li className="list-link">
                    <a href="#">Terms & Conditions</a>
                  </li>
                </ul>
              </div>
              <div className="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                <img
                  src={window.location.origin + "/assets/images/logo.png"}
                  className="logo"
                />
                <p className="desc">
                  N.A. Elixir-1, First Floor No 73/A Doddathogur Village, Begur
                  Hobli, Electronics City Phase 1, Bangalore, India.
                </p>
                <div className="ul-bottom__line mb-3">
                  <button
                    className="btn btn-facebook btn-icon m-1"
                    type="button"
                  >
                    <span className="ul-btn__icon">
                      <i className="i-Facebook-2"></i>
                    </span>
                  </button>
                  <button className="btn btn-google btn-icon m-1" type="button">
                    <span className="ul-btn__icon">
                      <i className="i-Google-Plus"></i>
                    </span>
                  </button>
                  <button
                    className="btn btn-twitter btn-icon m-1"
                    type="button"
                  >
                    <span className="ul-btn__icon">
                      <i className="i-Twitter"></i>
                    </span>
                  </button>
                  <button
                    className="btn btn-instagram btn-icon m-1"
                    type="button"
                  >
                    <span className="ul-btn__icon">
                      <i className="i-Instagram"></i>
                    </span>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="sub-footer-sec">
          <div className="container">
            <div className="row">
              <div className="col-md-12 text-center">
                <h3>© Copyright 2020 Codegama</h3>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default AuthFooter;
