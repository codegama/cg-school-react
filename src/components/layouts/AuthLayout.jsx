import React, { Component } from "react";
import AuthHeader from "./Header/AuthHeader";
import FooterIndex from "./Footer/FooterIndex";
import { Notify } from "react-redux-notify";
import AuthFooter from "./Footer/AuthFooter";

class AuthLayout extends Component {
  state = {};
  render() {
    return (
      <body>
        <AuthHeader />
        {React.cloneElement(this.props.children)}
        <AuthFooter />
      </body>
    );
  }
}

export default AuthLayout;
