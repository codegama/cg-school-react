import React from "react";

const InstructorTableSec = () => {
  return (
    <div class="row">
      <div class="col-xl-12 col-md-12">
        <div class="card o-hidden mb-4">
          <div class="card-header d-flex align-items-center bg-transparent border-0">
            <h3 class="w-50 float-left card-title m-0">User List</h3>
            <div class="search-bar">
              <input type="text" class="form-control" placeholder="Search" />
              <i class="search-icon text-muted i-Magnifi-Glass1"></i>
            </div>
          </div>
          <div>
            <div class="table-responsive instructor-table">
              <table class="table text-center" id="user_table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">User Name</th>
                    <th scope="col">Address</th>
                    <th scope="col">Date Of Joining</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th scope="row">1</th>
                    <td>
                      <a href="">
                        <div class="ul-widget-app__profile-pic">
                          <img
                            class="profile-picture avatar-sm mr-2 mb-2 rounded-circle img-fluid"
                            src={
                              window.location.origin +
                              "/assets/images/faces/1.jpg"
                            }
                            alt=""
                          />{" "}
                          Jhon Wick
                        </div>
                      </a>
                    </td>
                    <td>Banglore</td>
                    <td>18-07-2020</td>
                    <td>
                      <a class="text-success mr-2" href="#">
                        <i class="nav-icon i-Pen-2 font-weight-bold"></i>
                      </a>
                      <a class="text-danger mr-2" href="#">
                        <i class="nav-icon i-Close-Window font-weight-bold"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">2</th>
                    <td>
                      <a href="">
                        <div class="ul-widget-app__profile-pic">
                          <img
                            class="profile-picture avatar-sm mr-2 mb-2 rounded-circle img-fluid"
                            src={
                              window.location.origin +
                              "/assets/images/faces/1.jpg"
                            }
                            alt=""
                          />{" "}
                          Jhon Wick
                        </div>
                      </a>
                    </td>
                    <td>Chennai</td>
                    <td>25-08-2020</td>
                    <td>
                      <a class="text-success mr-2" href="#">
                        <i class="nav-icon i-Pen-2 font-weight-bold"></i>
                      </a>
                      <a class="text-danger mr-2" href="#">
                        <i class="nav-icon i-Close-Window font-weight-bold"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">3</th>
                    <td>
                      <a href="">
                        <div class="ul-widget-app__profile-pic">
                          <img
                            class="profile-picture avatar-sm  mr-2 mb-2 rounded-circle img-fluid"
                            src={
                              window.location.origin +
                              "/assets/images/faces/1.jpg"
                            }
                            alt=""
                          />{" "}
                          Jhon Wick
                        </div>
                      </a>
                    </td>
                    <td>Nagercoil</td>
                    <td>30-10-2020</td>
                    <td>
                      <a class="text-success mr-2" href="#">
                        <i class="nav-icon i-Pen-2 font-weight-bold"></i>
                      </a>
                      <a class="text-danger mr-2" href="#">
                        <i class="nav-icon i-Close-Window font-weight-bold"></i>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">4</th>
                    <td>
                      <a href="">
                        <div class="ul-widget-app__profile-pic">
                          <img
                            class="profile-picture avatar-sm mr-2 mb-2 rounded-circle img-fluid"
                            src={
                              window.location.origin +
                              "/assets/images/faces/1.jpg"
                            }
                            alt=""
                          />{" "}
                          Jhon Wick
                        </div>
                      </a>
                    </td>
                    <td>Coimbatore</td>
                    <td>20-07-2020</td>
                    <td>
                      <a class="text-success mr-2" href="#">
                        <i class="nav-icon i-Pen-2 font-weight-bold"></i>
                      </a>
                      <a class="text-danger mr-2" href="#">
                        <i class="nav-icon i-Close-Window font-weight-bold"></i>
                      </a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InstructorTableSec;
