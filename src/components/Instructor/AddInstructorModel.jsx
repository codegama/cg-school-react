import React from "react";

const AddInstructorModel = () => {
  return (
    <div
      class="modal fade"
      id="exampleModalCenter"
      tabindex="-1"
      role="dialog"
      aria-labelledby="exampleModalCenterTitle-2"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle-2">
              Add Instructor
            </h5>
            <button
              class="close"
              type="button"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
            <form>
              <div class="row">
                <div class="form-group col-md-6 mb-3">
                  <label for="firstName1">First name</label>
                  <input
                    class="form-control"
                    id="firstName1"
                    type="text"
                    placeholder="Enter your first name"
                  />
                </div>
                <div class="form-group col-md-6 mb-3">
                  <label for="lastName1">Last name</label>
                  <input
                    class="form-control"
                    id="lastName1"
                    type="text"
                    placeholder="Enter your last name"
                  />
                </div>
              </div>
              <div class="form-group mb-3">
                <label for="exampleInputEmail1">Email address</label>
                <input
                  class="form-control"
                  id="exampleInputEmail1"
                  type="email"
                  placeholder="Enter email"
                />
                <small id="emailHelp" class="form-text text-muted">
                  We'll never share your email with anyone else.
                </small>
              </div>
              <div class="form-group mb-3">
                <label for="phone">Phone(Optional)</label>
                <input
                  class="form-control"
                  id="phone"
                  placeholder="Enter phone"
                />
              </div>
              <div class="form-group mb-3">
                <label for="Password">Password</label>
                <input
                  class="form-control"
                  type="password"
                  id="Password"
                  placeholder="Enter Password"
                />
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button
              class="btn btn-secondary"
              type="button"
              data-dismiss="modal"
            >
              Close
            </button>
            <button class="btn btn-primary ml-2" type="button">
              Submit
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AddInstructorModel;
