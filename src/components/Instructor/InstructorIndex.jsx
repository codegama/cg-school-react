import React, { Component } from "react";
import InstructorTableSec from "./InstructorTableSec";
import AddInstructorModel from "./AddInstructorModel";

class InstructorIndex extends Component {
  state = {};
  render() {
    return (
      <section class="widgets-content">
        <div class="row mt-4">
          <div class="col-xl-12 col-md-12 mb-4">
            <div class="card mb-4 add-instructor">
              <div class="card-header d-flex align-items-center bg-transparent resp-justify-center">
                <h3 class="w-50 float-left card-title m-0">Add Instructor</h3>
              </div>
              <div class="card-body">
                <form>
                  <div class="form-group row mb-4">
                    <label class="col-sm-2 col-form-label" for="date-time">
                      Add instructor
                    </label>
                    <div class="col-sm-5">
                      <input
                        class="form-control"
                        id="date-time"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                        type="text"
                      />
                      <div
                        class="dropdown-menu instructor-dropdown"
                        x-placement="bottom-start"
                        style={{
                          position: "absolute",
                          transform: "translate3d(0px, 33px, 0px)",
                          top: "0px",
                          left: "0px",
                          willChange: "transform",
                        }}
                      >
                        <div class="ul-widget3-header mb-3">
                          <div class="ul-widget3-img">
                            <img
                              id="userDropdown"
                              src={
                                window.location.origin +
                                "/assets/images/faces/1.jpg"
                              }
                            />
                          </div>
                          <div class="ul-widget3-info">
                            <a class="__g-widget-username" href="#">
                              <span class="t-font-bolder">
                                Timithy Clarkson
                              </span>
                            </a>
                          </div>
                          <div class="add-btn ml-auto">
                            <button
                              class="btn btn-primary btn-sm m-1"
                              type="button"
                            >
                              <i class="fas fa-plus mr-2"></i>Add
                            </button>
                          </div>
                        </div>
                        <div class="ul-widget3-header">
                          <div class="ul-widget3-img">
                            <img
                              id="userDropdown"
                              src={
                                window.location.origin +
                                "/assets/images/faces/1.jpg"
                              }
                            />
                          </div>
                          <div class="ul-widget3-info">
                            <a class="__g-widget-username" href="#">
                              <span class="t-font-bolder">Beno darry</span>
                            </a>
                          </div>
                          <div class="add-btn ml-auto">
                            <button
                              class="btn btn-primary btn-sm m-1"
                              type="button"
                            >
                              <i class="fas fa-plus mr-2"></i>Add
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-5 text-center">
                      <button
                        class="btn btn-primary btn-lg add-i"
                        type="button"
                        data-toggle="modal"
                        data-target="#exampleModalCenter"
                      >
                        <i class="fas fa-plus mr-2"></i>Add Instructor
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <InstructorTableSec />
        <AddInstructorModel />
      </section>
    );
  }
}

export default InstructorIndex;
