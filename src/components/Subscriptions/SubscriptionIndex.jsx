import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchSubscriptionStart } from "../../store/actions/SubscriptionAction";

class SubscriptionIndex extends Component {
  state = {};
  componentDidMount() {
    if (this.props.subscriptions.subscription.loading)
      this.props.dispatch(fetchSubscriptionStart());
  }
  handleSubmit = (event, subscription) => {
    event.preventDefault();
    this.props.history.push(
      `/subscriptions-invoice/${subscription.subscription_id}`,
      subscription
    );
  };
  sendSubscriptionHistory = (event) => {
    event.preventDefault();
    this.props.history.push("/user/purchase/history");
  };
  render() {
    const { subscription } = this.props.subscriptions;
    return (
      <div className="main-content">
        <section className="ul-pricing-table">
          <div className="row">
            <div className="col-lg-12 col-xl-12">
              <div className="card">
                <div className="card-header bg-transparent">
                  <h4>Subscriptions</h4>
                </div>
                <div className="card-body mb-4">
                  <div className="row">
                    {subscription.loading
                      ? "Loading..."
                      : subscription.data.subscriptions.length > 0
                      ? subscription.data.subscriptions.map((sub) => (
                          <div className="col-lg-3 col-xl-3 m-0 p-0">
                            <div className="ul-pricing__table-1">
                              <div className="ul-pricing__header">
                                <div className="ul-pricing__main-number m-0">
                                  <h1
                                    className={
                                      sub.is_popular == 1
                                        ? "heading t-font-boldest text-success"
                                        : "heading t-font-boldest text-primary"
                                    }
                                  >
                                    {sub.subscription_amount_formatted}
                                  </h1>
                                </div>
                                <div className="ul-pricing__month">
                                  <small className="text-purple-100">
                                    per {sub.plan_formatted}
                                  </small>
                                </div>
                              </div>
                              <div className="ul-pricing__title">
                                <h2
                                  className={
                                    sub.is_popular == 1
                                      ? "heading text-success"
                                      : "heading text-primary"
                                  }
                                >
                                  {sub.title}
                                </h2>
                              </div>
                              <div className="ul-pricing__table-listing mb-4">
                                <ul>
                                  <li className="t-font-bolder">
                                    {sub.no_of_class} classes allowed
                                  </li>
                                  <li className="t-font-bolder">
                                    {" "}
                                    10 Instructors
                                  </li>
                                  <li className="t-font-bolder">
                                    {sub.no_of_users_each_class_formatted}
                                  </li>
                                </ul>
                              </div>
                              <div className="ul-pricing__desc">
                                <p className="text-muted text-center">
                                  {sub.description}
                                </p>
                              </div>
                              <button
                                className={
                                  sub.is_popular == 1
                                    ? "btn btn-lg btn-success btn-rounded m-1"
                                    : "btn btn-lg btn-primary btn-rounded m-1"
                                }
                                type="button"
                              >
                                Purchase
                              </button>
                            </div>
                          </div>
                        ))
                      : "No Data Found"}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToPros = (state) => ({
  subscriptions: state.subscriptions,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(mapStateToPros, mapDispatchToProps)(SubscriptionIndex);
