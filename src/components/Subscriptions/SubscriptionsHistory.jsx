import React, { Component } from "react";
import { Link } from "react-router-dom";

class SubscriptionHistory extends Component {
  state = {};
  render() {
    return (
      <div className="main-content">
        <section className="widgets-content">
          <div className="row mt-4">
            <div className="col-xl-12 col-md-12 mb-4">
              <div className="card">
                <div className="card-header flex-content bg-transparent">
                  <h4>My Subscription</h4>
                  <a href="my-subscription.html">
                    <Link
                      className="btn btn-primary width-200 btn-lg btn-md m-1"
                      to={"/subscriptions"}
                    >
                      Choose Plan
                    </Link>
                  </a>
                </div>
                <div className="card-body">
                  <div className="ul-widget__body">
                    <div className="tab-content">
                      <div
                        className="tab-pane active show"
                        id="ul-widget5-tab1-content"
                      >
                        <div className="ul-widget5">
                          <div className="ul-widget5__item">
                            <div className="ul-widget5__content">
                              <div className="ul-widget5__section width-100">
                                <h1 className="heading text-primary t-font-boldest">
                                  $0.00
                                </h1>
                                <p className="text-purple-100">per month</p>
                                <h2 className="heading text-primary">
                                  Free Plan
                                </h2>
                                <div className="ul-widget5__info mb-2">
                                  <span>Instructor:</span>
                                  <span className="text-primary"> You</span>
                                  <span>Created:</span>
                                  <span className="text-primary">
                                    26 July 2020
                                  </span>
                                </div>
                                <p className="text-muted width-450">
                                  Earum, quisquam, fugit? Numquam dolor magni
                                  nisi? Suscipit odit, ipsam iusto enim culpa
                                </p>
                              </div>
                            </div>
                            <div className="ul-widget5__content">
                              <div className="ul-widget5__section">
                                <div className="ul-widget5__info">
                                  <a
                                    href="#"
                                    className="btn btn-outline-danger btn-sm"
                                  >
                                    Cancel Subscription
                                  </a>
                                </div>
                              </div>
                            </div>
                            <div className="ul-widget5__content">
                              <div className="ul-widget5__section">
                                <a className="ul-widget4__title" href="#">
                                  <span>Expiry Date</span>
                                </a>
                                <p className="higlighted-txt">
                                  <span className="badge badge-pill higlighted-txt badge-primary p-2 m-1">
                                    8-Dec-2020
                                  </span>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="ul-widget__body">
                    <div className="tab-content">
                      <div
                        className="tab-pane active show"
                        id="ul-widget5-tab1-content"
                      >
                        <div className="ul-widget5">
                          <div className="ul-widget5__item">
                            <div className="ul-widget5__content">
                              <div className="ul-widget5__section width-100">
                                <h1 className="heading text-primary t-font-boldest">
                                  $0.00
                                </h1>
                                <p className="text-purple-100">per month</p>
                                <h2 className="heading text-primary">
                                  Free Plan
                                </h2>
                                <div className="ul-widget5__info mb-2">
                                  <span>Instructor:</span>
                                  <span className="text-primary"> You</span>
                                  <span>Created:</span>
                                  <span className="text-primary">
                                    26 July 2020
                                  </span>
                                </div>
                                <p className="text-muted width-450">
                                  Earum, quisquam, fugit? Numquam dolor magni
                                  nisi? Suscipit odit, ipsam iusto enim culpa
                                </p>
                              </div>
                            </div>
                            <div className="ul-widget5__content">
                              <div className="ul-widget5__section">
                                <div className="ul-widget5__info">
                                  <a
                                    href="#"
                                    className="btn btn-outline-danger btn-sm"
                                  >
                                    Cancel Subscription
                                  </a>
                                </div>
                              </div>
                            </div>
                            <div className="ul-widget5__content">
                              <div className="ul-widget5__section">
                                <a className="ul-widget4__title" href="#">
                                  <span>Expiry Date</span>
                                </a>
                                <p className="higlighted-txt">
                                  <span className="badge badge-pill higlighted-txt badge-primary p-2 m-1">
                                    8-Dec-2020
                                  </span>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="ul-widget__body">
                    <div className="tab-content">
                      <div
                        className="tab-pane active show"
                        id="ul-widget5-tab1-content"
                      >
                        <div className="ul-widget5">
                          <div className="ul-widget5__item">
                            <div className="ul-widget5__content">
                              <div className="ul-widget5__section width-100">
                                <h1 className="heading text-primary t-font-boldest">
                                  $0.00
                                </h1>
                                <p className="text-purple-100">per month</p>
                                <h2 className="heading text-primary">
                                  Free Plan
                                </h2>
                                <div className="ul-widget5__info mb-2">
                                  <span>Instructor:</span>
                                  <span className="text-primary"> You</span>
                                  <span>Created:</span>
                                  <span className="text-primary">
                                    26 July 2020
                                  </span>
                                </div>
                                <p className="text-muted width-450">
                                  Earum, quisquam, fugit? Numquam dolor magni
                                  nisi? Suscipit odit, ipsam iusto enim culpa
                                </p>
                              </div>
                            </div>
                            <div className="ul-widget5__content">
                              <div className="ul-widget5__section">
                                <div className="ul-widget5__info">
                                  <a
                                    href="#"
                                    className="btn btn-outline-danger btn-sm"
                                  >
                                    Cancel Subscription
                                  </a>
                                </div>
                              </div>
                            </div>
                            <div className="ul-widget5__content">
                              <div className="ul-widget5__section">
                                <a className="ul-widget4__title" href="#">
                                  <span>Expiry Date</span>
                                </a>
                                <p className="higlighted-txt">
                                  <span className="badge badge-pill higlighted-txt badge-primary p-2 m-1">
                                    8-Dec-2020
                                  </span>
                                </p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default SubscriptionHistory;
