import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const settings = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 1,
  accessibility: true,
  arrows: true,
};

class LandingPageIndex extends Component {
  state = {};
  render() {
    return (
      <>
        <section class="sm-padding features-sec">
          <div class="container">
            <div class="row">
              <div class="col-md-12  text-center">
                <h1 class="section-title">Features</h1>
              </div>
            </div>
            <div class="row flex-row-reverse">
              <div class="col-lg-9 col-md-12 col-sm-12">
                <div class="row mb-4">
                  <div class="col-lg-3 col-md-3 col-sm-12 resp-mrg-btm-sm">
                    <a href="#" class="features-box">
                      <img
                        src={
                          window.location.origin +
                          "/assets/images/landing-page/chat.png"
                        }
                        alt="Chat"
                      />
                      <h5 class="title">Chat</h5>
                      <p class="px-2 desc">Send public and private messages</p>
                    </a>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12 resp-mrg-btm-sm ">
                    <a href="#" class="features-box">
                      <img
                        src={
                          window.location.origin +
                          "/assets/images/landing-page/video.png"
                        }
                        alt="Video"
                      />
                      <h5 class="title">Video</h5>
                      <p class="px-2 desc">
                        1-on-1 or multiple video attendees
                      </p>
                    </a>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12 resp-mrg-btm-sm ">
                    <a href="#" class="features-box">
                      <img
                        src={
                          window.location.origin +
                          "/assets/images/landing-page/mic.png"
                        }
                        alt="Audio"
                      />
                      <h5 class="title">Audio</h5>
                      <p class="px-2 desc">
                        Audio only attendees or entire meetings
                      </p>
                    </a>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                    <a href="#" class="features-box">
                      <img
                        src={
                          window.location.origin +
                          "/assets/images/landing-page/screen.png"
                        }
                        alt="Screen Sharing"
                      />
                      <h5 class="title">Screen Sharing</h5>
                      <p class="px-2 desc">Share your screen with attendees</p>
                    </a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-3 col-sm-12 resp-mrg-btm-sm ">
                    <a href="#" class="features-box">
                      <img
                        src={
                          window.location.origin +
                          "/assets/images/landing-page/cloudupload.png"
                        }
                        alt="Record"
                      />
                      <h5 class="title">Record</h5>
                      <p class="px-2 desc">Record your meetings in cloud</p>
                    </a>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12 resp-mrg-btm-sm ">
                    <a href="#" class="features-box">
                      <img
                        src={
                          window.location.origin +
                          "/assets/images/landing-page/whiteboard2.png"
                        }
                        alt="Whiteboard"
                      />
                      <h5 class="title">Whiteboard</h5>
                      <p class="px-2 desc">
                        Add presentations, annotate or draw
                      </p>
                    </a>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12 resp-mrg-btm-sm ">
                    <a href="#" class="features-box">
                      <img
                        src={
                          window.location.origin +
                          "/assets/images/landing-page/collab.png"
                        }
                        alt="Notes"
                      />
                      <h5 class="title">Collaborative Notes</h5>
                      <p class="px-2 desc">
                        Realtime multi user notes together
                      </p>
                    </a>
                  </div>
                  <div class="col-lg-3 col-md-3 col-sm-12">
                    <a href="#" class="features-box">
                      <img
                        src={
                          window.location.origin +
                          "/assets/images/landing-page/poll.png"
                        }
                        alt="Polls"
                      />
                      <h5 class="title">Polls</h5>
                      <p class="px-2 desc">
                        Quick opinion polls from attendees
                      </p>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="sm-padding business-sec">
          <div class="container">
            <div class="row">
              <div class="col-md-6 col-sm-12 col-xs-12">
                <div class=" business-info">
                  <h3 class="title">Business-grade Virtual Conference Halls</h3>
                  <p class="desc">
                    Conduct professional-level highly interactive business
                    conferences with people around the world using our Zoom
                    clone script.
                  </p>
                  <div class="row">
                    <div class="col-md-2 resp-mrg-btm-sm">
                      <img
                        src={
                          window.location.origin +
                          "/assets/images/landing-page/calendar.png"
                        }
                        class="img-fluid"
                      />
                    </div>
                    <div class="col-md-10">
                      <h4 class="sub-title">Schedule Meetings</h4>
                      <p class="desc">
                        Our Zoom clone script lets you keep check of your
                        important meetings and engagements right inside your
                        business’s ecosystem. Schedule meetings and sync the
                        data with any of your organizer apps with this robust
                        video conferencing solution.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12">
                <img
                  src={
                    window.location.origin +
                    "/assets/images/landing-page/banner-img-1.png"
                  }
                />
              </div>
            </div>
          </div>
        </section>
        <section class="testimonial-sec sm-padding">
          <div class="container">
            <div class="row">
              <div class="col-md-12  text-center">
                <h1 class="section-title">Testimonial</h1>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="testimonial-card">
                  <div class="testimonial-list">
                    <Slider {...settings}>
                      <div class="testimonial-item">
                        <div class="inner">
                          <div class="testimonial-box">
                            <img
                              src={
                                window.location.origin +
                                "/assets/images/landing-page/2.jpg"
                              }
                              class="client-img"
                            />
                            <div class="details">
                              <h4 class="client-name">Angel Witicker</h4>
                              <h5 class="client-role">
                                Head of Marketing, Extra Space
                              </h5>
                              <p class="sub-desc">
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="testimonial-item">
                        <div class="inner">
                          <div class="testimonial-box">
                            <img
                              src={
                                window.location.origin +
                                "/assets/images/landing-page/9.jpg"
                              }
                              class="client-img"
                            />
                            <div class="details">
                              <h4 class="client-name">Debra Becker</h4>
                              <h5 class="client-role">
                                Founder & CEO, Divi Corner
                              </h5>
                              <p class="sub-desc">
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="testimonial-item">
                        <div class="inner">
                          <div class="testimonial-box">
                            <img
                              src={
                                window.location.origin +
                                "/assets/images/landing-page/10.jpg"
                              }
                              class="client-img"
                            />
                            <div class="details">
                              <h4 class="client-name">John Wise</h4>
                              <h5 class="client-role">
                                Head of Design, Elegant Themes
                              </h5>
                              <p class="sub-desc">
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="testimonial-item">
                        <div class="inner">
                          <div class="testimonial-box">
                            <img
                              src={
                                window.location.origin +
                                "/assets/images/landing-page/13.jpg"
                              }
                              class="client-img"
                            />
                            <div class="details">
                              <h4 class="client-name">Angel Witicker</h4>
                              <h5 class="client-role">
                                Head of Marketing, Extra Space
                              </h5>
                              <p class="sub-desc">
                                Lorem ipsum dolor sit amet, consectetur
                                adipiscing elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Slider>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section class="sm-padding call-to-action">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <div class="call-to-action-info">
                  <h3 class="title">
                    Replace your Video Conferencing with our Zoom Clone software
                  </h3>
                  <p class="desc">
                    Your business deserves to get ahead contact our experts to
                    find out how.
                  </p>
                  <div class="double-btn-sec">
                    <button class="btn btn-primary width-200 mr-2 resp-mr-2">
                      Get Started Now
                    </button>
                    <button class="btn btn-outline-secondary width-200  m-1">
                      Book a Demo
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </>
    );
  }
}

export default LandingPageIndex;
