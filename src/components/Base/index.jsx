import React, { Component } from "react";
import { createBrowserHistory as createHistory } from "history";
import { Router, Switch, Route, Redirect } from "react-router-dom";
import Logout from "../Auth/Logout";
import MainLayout from "../layouts/MainLayout";
import DashboardIndex from "../Dashboard/DashboardIndex";
import HomeIndex from "../Home/HomeIndex";
import LoginIndex from "../Auth/LoginIndex";
import AuthLayout from "../layouts/AuthLayout";
import RegisterIndex from "../Auth/RegisterIndex";
import ForgotPassword from "../Auth/ForgotPassword";
import SubscriptionIndex from "../Subscriptions/SubscriptionIndex";
import CardsList from "../Accounts/Cards/CardsList";
import EditProfile from "../Accounts/Profile/EditProfile";
import DeleteAccountIndex from "../Accounts/DeleteAccount/DeleteAccountIndex";
import ChangePasswordIndex from "../Accounts/ChangePassword/ChangePasswordIndex";
import ProfileIndex from "../Accounts/Profile/ProfileIndex";
import InstructorIndex from "../Instructor/InstructorIndex";
import RoomIndex from "../Room/RoomIndex";
import CreateRoomIndex from "../Room/CreateRoomIndex";
import SingleRoomIndex from "../Room/SingleRoom/SingleRoomIndex";
import CreateMeetingIndex from "../Meeting/CreateMeetingIndex";
import SingleMeetingIndex from "../Meeting/SingleMeeting/SingleMeetingIndex";
import SubscriptionHistory from "../Subscriptions/SubscriptionsHistory";
import LandingPageIndex from "../LandingPage/LandingPageIndex";

const history = createHistory();
const $ = window.$;

const AppRoute = ({
  component: Component,
  layout: Layout,
  screenProps: ScreenProps,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) => (
      <Layout screenProps={ScreenProps} {...props}>
        <Component {...props} />
      </Layout>
    )}
    isAuthed
  />
);

const PrivateRoute = ({
  component: Component,
  layout: Layout,
  screenProps: ScreenProps,
  authentication,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) =>
      authentication === true ? (
        <Layout screenProps={ScreenProps}>
          <Component {...props} authRoute={true} />
        </Layout>
      ) : (
        <Redirect to={{ pathname: "/", state: { from: props.location } }} />
      )
    }
  />
);

class App extends Component {
  // constructor(props) {
  //   super(props);
  //   let userId = localStorage.getItem("userId");
  //   let accessToken = localStorage.getItem("accessToken");
  //   this.state = {
  //     loading: true,
  //     configLoading: true,
  //     authentication: userId && accessToken ? true : false,
  //   };

  //   history.listen((location, action) => {
  //     userId = localStorage.getItem("userId");

  //     accessToken = localStorage.getItem("accessToken");

  //     this.setState({
  //       loading: true,
  //       authentication: userId && accessToken ? true : false,
  //     });

  //     document.body.scrollTop = 0;
  //   });
  //   this.fetchConfig();
  // }

  // async fetchConfig() {
  //   const response = await fetch(apiConstants.settingsUrl);
  //   const configValue = await response.json();

  //   configuration.set({ configData: configValue.data }, { freeze: false });
  //   this.setState({ configLoading: false });
  // }

  render() {
    // const isLoading = this.state.configLoading;

    // if (isLoading) {
    //   return (
    //     <div className="wrapper">
    //       <div className="loader-warpper">
    //         <div id="loader" className="page-loader">
    //           <p>If I’m not back in five minutes, just wait longer.</p>
    //         </div>
    //       </div>
    //     </div>
    //   );
    // }

    return (
      <>
        {/* <Helmet>
          <title>{configuration.get("configData.site_name")}</title>
          <link
            rel="icon"
            type="image/png"
            href={configuration.get("configData.site_icon")}
            sizes="16x16"
          />
          <meta
            name="description"
            content={configuration.get("configData.meta_description")}
          ></meta>
          <meta
            name="keywords"
            content={configuration.get("configData.meta_keywords")}
          ></meta>
          <meta
            name="author"
            content={configuration.get("configData.meta_author")}
          ></meta>
        </Helmet> */}

        <Switch>
          <AppRoute
            path={"/"}
            component={LandingPageIndex}
            exact
            layout={AuthLayout}
          />
          <AppRoute
            path={"/dashboard"}
            component={DashboardIndex}
            layout={MainLayout}
          />

          <AppRoute
            path={"/instructor"}
            component={InstructorIndex}
            layout={MainLayout}
          />

          <AppRoute
            path={"/room-management"}
            component={RoomIndex}
            layout={MainLayout}
          />

          <AppRoute
            path={"/create-room/"}
            component={CreateRoomIndex}
            layout={MainLayout}
          />

          <AppRoute
            path={"/create-meeting/:id"}
            component={CreateMeetingIndex}
            layout={MainLayout}
          />

          <AppRoute
            path={"/single-meeting/:id"}
            component={SingleMeetingIndex}
            layout={MainLayout}
          />

          <AppRoute
            path={"/single-room/:id"}
            component={SingleRoomIndex}
            layout={MainLayout}
          />

          <AppRoute
            path={"/edit-profile"}
            component={EditProfile}
            layout={MainLayout}
          />

          <AppRoute
            path={"/profile"}
            component={ProfileIndex}
            layout={MainLayout}
          />

          <AppRoute
            path={"/delete-account"}
            component={DeleteAccountIndex}
            layout={MainLayout}
          />

          <AppRoute
            path={"/change-password"}
            component={ChangePasswordIndex}
            layout={MainLayout}
          />

          <AppRoute
            path={"/subscriptions"}
            component={SubscriptionIndex}
            layout={MainLayout}
          />

          <AppRoute
            path={"/my-subscriptions"}
            component={SubscriptionHistory}
            layout={MainLayout}
          />
          <Route path={"/login"} component={LoginIndex} />

          <Route path={"/signup"} component={RegisterIndex} />
          <Route path={"/forgot-password"} component={ForgotPassword} />

          <AppRoute path={"/cards"} component={CardsList} layout={MainLayout} />

          <AppRoute path={"/logout"} component={Logout} layout={MainLayout} />
        </Switch>
      </>
    );
  }
}

export default App;
