import React, { Component } from "react";
import { MDBContainer, MDBRow, MDBCol, MDBInput, MDBBtn } from "mdbreact";

class StaticIndex extends Component {
  state = {};
  render() {
    return (
      <div>
        <div className="static-page-sec sm-padding">
          <div className="container">
            <div className="row">
              <div className="col-md-12">
                <div className="card">
                  <div className="card-header">
                    <h5 className="card-title">About Us</h5>
                  </div>
                  <div className="card-body">
                    <div className="main-section">
                      <h2 class="header">Introduction</h2>
                      <p>
                        The greatest, most sustainable happiness comes from
                        making others happy. It is our privilege to deliver you
                        happiness every single day.
                      </p>
                      <p>
                        Zoom helps businesses and organizations bring their
                        teams together in a frictionless environment to get more
                        done. Our easy, reliable cloud platform for video,
                        voice, content sharing, and chat runs across mobile
                        devices, desktops, telephones, and room systems. Zoom is
                        publicly traded on Nasdaq (ZM) and headquartered in San
                        Jose, California.
                      </p>
                      <h2 class="header">How does Zoom Work?</h2>
                      <ul>
                        <li>
                          <p>
                            Our mission - make video communications frictionless
                          </p>
                        </li>
                        <li>
                          <p>
                            Our Vision - video communications empowering people
                            to accomplish more
                          </p>
                        </li>
                        <li>
                          <p>
                            Our Values - we care for our company, customers,
                            community, teammates, and ourselves
                          </p>
                        </li>
                      </ul>
                      <h2 class="header">Lorem Ipsum</h2>
                      <p>
                        Zoom helps businesses and organizations bring their
                        teams together in a frictionless environment to get more
                        done. Our easy, reliable cloud platform for video,
                        voice, content sharing, and chat runs across mobile
                        devices, desktops, telephones, and room systems. Zoom is
                        publicly traded on Nasdaq (ZM) and headquartered in San
                        Jose, California.
                      </p>
                      <p>
                        GARTNER is a registered trademark and service mark of
                        Gartner, Inc. and/or its affiliates in the U.S. and
                        internationally, and is used herein with permission. All
                        rights reserved.
                      </p>
                      <p>
                        Zoom is unique. It’s not often you find a company that
                        values their customers as well as employees while
                        delivering a state of the art product
                      </p>
                      <p>
                        You know what I love about Zoom, it's constant growth,
                        constant fun, and constant change. I work with the
                        smartest and best people that I could ever be surrounded
                        with!
                      </p>
                      <h2 class="header">Lorem Ipsum</h2>
                      <p>
                        Zoom helps businesses and organizations bring their
                        teams together in a frictionless environment to get more
                        done. Our easy, reliable cloud platform for video,
                        voice, content sharing, and chat runs across mobile
                        devices, desktops, telephones, and room systems. Zoom is
                        publicly traded on Nasdaq (ZM) and headquartered in San
                        Jose, California.
                      </p>
                      <p>
                        GARTNER is a registered trademark and service mark of
                        Gartner, Inc. and/or its affiliates in the U.S. and
                        internationally, and is used herein with permission. All
                        rights reserved.
                      </p>
                      <p>
                        Zoom is unique. It’s not often you find a company that
                        values their customers as well as employees while
                        delivering a state of the art product
                      </p>
                      <p>
                        You know what I love about Zoom, it's constant growth,
                        constant fun, and constant change. I work with the
                        smartest and best people that I could ever be surrounded
                        with!
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default StaticIndex;
