import React, { Component } from "react";
import SingleMeetingUserList from "./SingleMeetingUserList";
import { connect } from "react-redux";
import { fetchSingleMeetingStart } from "../../../store/actions/MeetingAction";

class SingleMeetingIndex extends Component {
  state = {
    userDisplay: "none",
  };
  componentDidMount() {
    this.props.dispatch(
      fetchSingleMeetingStart({ meeting_id: this.props.match.params.id })
    );
  }

  changeDisplayStatus = (status) => {
    if (status == "view") this.setState({ userDisplay: "block" });
    if (status == "none") this.setState({ userDisplay: "none" });
  };
  render() {
    const { singleMeeting } = this.props;
    return (
      <div className="main-content single-meeting">
        <section className="widgets-content">
          <div className="row mt-4">
            <div
              className={
                this.state.userDisplay == "none"
                  ? "display-column-lg"
                  : "display-column-lg display-column-md resp-mrg-btm-md"
              }
            >
              <div className="card">
                <div className="card-header flex-content bg-transparent">
                  <h4>Single Meeting View</h4>
                  <button
                    className="btn btn-primary btn-lg width-200 btn-md m-1"
                    type="button"
                  >
                    {" "}
                    Start Meeting
                  </button>
                </div>
                {singleMeeting.loading ? (
                  "Loading..."
                ) : (
                  <div className="col-lg-12 col-xl-12 mb-3">
                    <div className="card-body">
                      <div className="ul-widget5">
                        <div className="ul-widget5__item">
                          <div className="ul-widget5__content">
                            <div className="ul-widget5__pic single-meeting-pic">
                              <img
                                src={
                                  singleMeeting.data.data.meeting_details
                                    .picture
                                }
                                alt="Third slide"
                                className="single-meeting-img"
                              />
                            </div>
                            <div className="ul-widget5__section resp-padding-top-md">
                              <a className="ul-widget4__title" href="#">
                                {singleMeeting.data.data.meeting_details.title}
                              </a>
                              <p className="ul-widget5__desc">
                                {
                                  singleMeeting.data.data.meeting_details
                                    .description
                                }
                              </p>
                              <div className="ul-widget5__info">
                                <span>Date</span>
                                <span className="text-primary">
                                  26 July 2020
                                </span>
                                <span>Time</span>
                                <span className="text-primary">8:45 PM</span>
                              </div>
                            </div>
                          </div>
                          <div className="ul-widget5__content mx-auto">
                            <div className="ul-widget5__section">
                              <a className="ul-widget4__title" href="#">
                                <span>Meeting Type:</span>
                                <span className="text-primary">Live</span>
                              </a>
                              <p className="ul-widget5__desc">20 Users</p>
                              {this.state.userDisplay != "none" ? (
                                <a
                                  href="#"
                                  className="btn btn-primary btn-sm view-user"
                                  onClick={() =>
                                    this.changeDisplayStatus("none")
                                  }
                                >
                                  Close User
                                </a>
                              ) : (
                                <a
                                  href="#"
                                  className="btn btn-primary btn-sm view-user"
                                  onClick={() =>
                                    this.changeDisplayStatus("view")
                                  }
                                >
                                  View User
                                </a>
                              )}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="video-sec">
                        <video width="700" height="400" controls>
                          <source
                            src="https://www.youtube.com/watch?v=BC56rj2ZQSY"
                            type="video/mp4"
                          />
                        </video>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
            <SingleMeetingUserList userDisplay={this.state.userDisplay} />
          </div>
        </section>
      </div>
    );
  }
}

const mapStateToPros = (state) => ({
  singleMeeting: state.meeting.singleMeeting,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(mapStateToPros, mapDispatchToProps)(SingleMeetingIndex);
