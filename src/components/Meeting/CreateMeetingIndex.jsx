import React, { Component } from "react";
import { connect } from "react-redux";
import { addMeetingStart } from "../../store/actions/MeetingAction";

class CreateMeetingIndex extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputData: {
        room_id: props.match.params.id,
      },
      meetingType: "upload",
    };
  }

  handleChange = ({ currentTarget: input }) => {
    const inputData = { ...this.state.inputData };
    inputData[input.name] = input.value;
    this.setState({ inputData });
    if (input.type == "radio") {
      this.setState({ meetingType: input.value });
    }
  };
  handleSubmit = (event) => {
    event.preventDefault();
    this.props.dispatch(addMeetingStart(this.state.inputData));
  };

  handleChangeImage = ({ currentTarget: input }) => {
    const inputData = { ...this.state.inputData };
    if (input.type === "file") {
      inputData[input.name] = input.files[0];
      this.setState({ inputData });
    }
    let reader = new FileReader();
    let file = input.files[0];

    reader.onloadend = () => {
      if (input.name == "picture")
        this.setState({
          imagePreviewUrl: reader.result,
        });
    };
    if (file) {
      reader.readAsDataURL(file);
    }
  };

  render() {
    const { inputData } = this.state;
    return (
      <section class="widgets-content">
        <div class="row mt-4">
          <div class="col-xl-12 col-md-12 mb-4">
            <div class="card">
              <div class="card-header bg-transparent">
                <h4>Create Meeting</h4>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-lg-6 col-md-7 col-sm-12 col-xs-12">
                    <form onSubmit={this.handleSubmit}>
                      <div class="form-group row">
                        <label
                          class="col-sm-3 col-form-label"
                          for="inputEmail3"
                        >
                          Title
                        </label>
                        <div class="col-sm-9">
                          <input
                            class="form-control"
                            id="inputEmail3"
                            type="text"
                            value={inputData.title}
                            name="title"
                            onChange={this.handleChange}
                          />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-sm-3 col-form-label" for="desc">
                          Description
                        </label>
                        <div class="col-sm-9">
                          <textarea
                            class="form-control"
                            id="desc"
                            type="text"
                            value={inputData.description}
                            name="description"
                            onChange={this.handleChange}
                          ></textarea>
                        </div>
                      </div>
                      <fieldset class="form-group text-left">
                        <div class="row">
                          <div class="col-form-label col-lg-3 col-md-3 col-sm-12 pt-0 align-center">Radios</div>
                          <div class="col-lg-9 col-md-9 col-sm-12 flex-center">
                            <ul class="list-unstyled mb-0">
                              <li class="mb-2">
                                <div>
                                  <label class="radio radio-primary">
                                    <input
                                      type="radio"
                                      name="radio"
                                      value="upload"
                                      name="meeting_type"
                                      onChange={this.handleChange}
                                      defaultChecked={true}
                                    />
                                    <span>Upload Video</span>
                                    <span class="checkmark"></span>
                                  </label>
                                </div>
                              </li>
                              <li>
                                <div>
                                  <label class="radio radio-primary">
                                    <input
                                      type="radio"
                                      name="radio"
                                      value="live"
                                      name="meeting_type"
                                      onChange={this.handleChange}
                                    />
                                    <span>Live Streaming</span>
                                    <span class="checkmark"></span>
                                  </label>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </fieldset>
                      {this.state.meetingType == "live" ? (
                        <div class="form-group row">
                          <label
                            class="col-sm-3 col-form-label"
                            for="date-time"
                          >
                            Date & Time
                          </label>
                          <div class="col-sm-9">
                            <input
                              class="form-control"
                              id="date-time"
                              type="text"
                            />
                          </div>
                        </div>
                      ) : null}
                      <div class="form-group row">
                        <label
                          class="col-sm-3 col-form-label"
                          for="inputEmail3"
                        >
                          Upload Image
                        </label>
                        <div class="col-sm-9">
                          <div class="upload-btn-wrapper create-meeting">
                            <button class="upload-btn">Upload a image</button>
                            <input
                              type="file"
                              name="picture"
                              onChange={this.handleChangeImage}
                            />
                          </div>
                        </div>
                      </div>
                      {this.state.meetingType == "upload" ? (
                        <div class="form-group row">
                          <label
                            class="col-sm-3 col-form-label"
                            for="inputEmail3"
                          >
                            Upload Video
                          </label>
                          <div class="col-sm-9">
                            <div class="upload-btn-wrapper create-meeting">
                              <button class="upload-btn">Upload a video</button>
                              <input type="file" name="myfile" />
                            </div>
                          </div>
                        </div>
                      ) : null}
                      <div class="form-group row mb-4">
                        <label class="col-sm-3 col-form-label" for="date-time">
                          Instructor
                        </label>
                        <div class="col-sm-9">
                          <input
                            class="form-control"
                            id="date-time"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                            type="text"
                          />
                          <div
                            class="dropdown-menu instructor-dropdown"
                            x-placement="bottom-start"
                            style={{
                              position: "absolute",
                              transform: "translate3d(0px, 33px, 0px)",
                              top: "0px",
                              left: "0px",
                              willChange: "transform",
                            }}
                          >
                            <div class="ul-widget3-header mb-3">
                              <div class="ul-widget3-img">
                                <img
                                  id="userDropdown"
                                  src={
                                    window.location.origin +
                                    "/assets/images/faces/1.jpg"
                                  }
                                />
                              </div>
                              <div class="ul-widget3-info">
                                <a class="__g-widget-username" href="#">
                                  <span class="t-font-bolder">
                                    Timithy Clarkson
                                  </span>
                                </a>
                              </div>
                              <div class="add-btn ml-auto">
                                <button
                                  class="btn btn-primary btn-sm m-1"
                                  type="button"
                                >
                                  <i class="fas fa-plus mr-2"></i>Add
                                </button>
                              </div>
                            </div>
                            <div class="ul-widget3-header">
                              <div class="ul-widget3-img">
                                <img
                                  id="userDropdown"
                                  src={
                                    window.location.origin +
                                    "/assets/images/faces/1.jpg"
                                  }
                                />
                              </div>
                              <div class="ul-widget3-info">
                                <a class="__g-widget-username" href="#">
                                  <span class="t-font-bolder">Beno darry</span>
                                </a>
                              </div>
                              <div class="add-btn ml-auto">
                                <button
                                  class="btn btn-primary btn-sm m-1"
                                  type="button"
                                >
                                  <i class="fas fa-plus mr-2"></i>Add
                                </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group row">
                        <div class="col-sm-3"></div>
                        <div class="col-sm-9">
                          <button
                            class="btn btn-block btn-primary"
                            type="submit"
                            disabled={this.props.meeting.buttonDisable}
                          >
                            {this.props.meeting.loadingButtonContent != null
                              ? this.props.meeting.loadingButtonContent
                              : "Create Meeting"}
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="col-lg-6 col-md-5 col-sm-12 col-xs-12">
                    <img
                      src={
                        window.location.origin +
                        "/assets/images/create-meeting.svg"
                      }
                      class="img-fluid create-meeting-img"
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToPros = (state) => ({
  meeting: state.meeting,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(mapStateToPros, mapDispatchToProps)(CreateMeetingIndex);
