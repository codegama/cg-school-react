import React, { Component } from "react";
import { connect } from "react-redux";
import { userRegisterStart } from "../../store/actions/UserAction";
import { Link } from "react-router-dom";

class RegisterIndex extends Component {
  state = {
    inputData: {},
  };

  handleChange = ({ currentTarget: input }) => {
    const inputData = { ...this.state.inputData };
    inputData[input.name] = input.value;
    this.setState({ inputData });
  };

  handleRegisterSubmit = (event) => {
    event.preventDefault();
    this.props.dispatch(userRegisterStart(this.state.inputData));
  };

  render() {
    const { inputData } = this.state;
    return (
      <div class="app-admin-wrap layout-sidebar-large">
        <div class="signup-box">
          <div class="main-content">
            <div class="row">
              <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 mx-auto">
                <div class="auth-content">
                  <div class="card o-hidden">
                    <div class="row">
                      <div
                        class="col-md-6 text-center login-img"
                        style={{
                          backgroundSize: "cover",
                          backgroundImage: `url(${window.location.origin}/assets/images/register.jpg)`,
                        }}
                      ></div>
                      <div class="col-md-6">
                        <div class="p-4">
                          <div class="card-title mb-3 text-center">
                            Welcome to back to CG School
                          </div>
                          <p class="text-muted text-center">
                            CodeGama - Code of Conduct is Guaranteed | Tech
                            Coder
                          </p>
                          <form onSubmit={this.handleRegisterSubmit}>
                            <div class="form-group mb-3">
                              <label for="firstName1">Name</label>
                              <input
                                className="form-control"
                                id="firstName1"
                                type="text"
                                placeholder="Enter your name"
                                name="name"
                                value={inputData.name}
                                onChange={this.handleChange}
                              />
                            </div>
                            <div class="form-group mb-3">
                              <label for="exampleInputEmail1">
                                Email address
                              </label>
                              <input
                                className="form-control"
                                id="exampleInputEmail1"
                                type="email"
                                placeholder="Enter email"
                                name="email"
                                value={inputData.email}
                                onChange={this.handleChange}
                              />
                              <small
                                id="emailHelp"
                                class="form-text text-muted"
                              >
                                We'll never share your email with anyone else.
                              </small>
                            </div>
                            <div class="form-group mb-3">
                              <label for="phone">Phone(Optional)</label>
                              <input
                                className="form-control"
                                id="phone"
                                placeholder="Enter phone"
                                name="mobile"
                                value={inputData.mobile}
                                onChange={this.handleChange}
                              />
                            </div>
                            <div class="form-group mb-3">
                              <label for="Password">Password</label>
                              <input
                                className="form-control"
                                type="password"
                                id="Password"
                                placeholder="Enter Password"
                                name="password"
                                value={inputData.password}
                                onChange={this.handleChange}
                              />
                            </div>
                            <div class="form-group">
                              <div class="form-check">
                                <input
                                  class="form-check-input"
                                  id="invalidCheck"
                                  type="checkbox"
                                  required="required"
                                />
                                <label
                                  class="form-check-label"
                                  for="invalidCheck"
                                >
                                  Agree to <a href="">Terms conditions</a> and{" "}
                                  <a href="">Privacy Policy</a>
                                </label>
                                <div class="invalid-feedback">
                                  You must agree before submitting.
                                </div>
                              </div>
                            </div>
                            <div class="col-md-12">
                              <button
                                type="submit"
                                className="btn btn-lg btn-primary btn-block"
                                disabled={this.props.inputData.buttonDisable}
                              >
                                {this.props.inputData.loadingButtonContent !=
                                null
                                  ? this.props.inputData.loadingButtonContent
                                  : "Submit"}
                              </button>
                            </div>
                          </form>
                          <div class="row mt-3 text-center">
                            <div class="col-md-12 col-xs-12 col-sm-12">
                              <Link className="text-muted" to={"/login"}>
                                <u>Already have an Account?</u>
                              </Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToPros = (state) => ({
  inputData: state.users,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(mapStateToPros, mapDispatchToProps)(RegisterIndex);
