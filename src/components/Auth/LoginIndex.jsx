import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getLoginInputData,
  userLoginStart,
} from "../../store/actions/UserAction";
import { Link } from "react-router-dom";

class LoginIndex extends Component {
  state = {
    inputData: {},
  };

  handleChange = ({ currentTarget: input }) => {
    const inputData = { ...this.state.inputData };
    inputData[input.name] = input.value;
    this.setState({ inputData });
  };
  handleLoginSubmit = (event) => {
    event.preventDefault();
    this.props.dispatch(userLoginStart(this.state.inputData));
  };

  render() {
    const { inputData } = this.state;
    return (
      <div class="app-admin-wrap layout-sidebar-large">
        <div class="login-box">
          <div class="main-content">
            <div class="row">
              <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 mx-auto">
                <div class="auth-content">
                  <div class="card">
                    <div class="row">
                      <div
                        class="col-md-6 col-sm-12 text-center login-img"
                        style={{
                          backgroundSize: "cover",
                          backgroundImage: `url(${window.location.origin}/assets/images/login.jpg)`,
                        }}
                      ></div>
                      <div class="col-md-6 col-sm-12">
                        <div class="p-4">
                          <div class="card-title mb-3 text-center">
                            Welcome to back to CG School
                          </div>
                          <p class="text-muted text-center">
                            CodeGama - Code of Conduct is Guaranteed | Tech
                            Coder
                          </p>
                          <form onSubmit={this.handleLoginSubmit}>
                            <div class="form-group mb-3">
                              <label for="exampleInputEmail1">
                                Email address
                              </label>
                              <input
                                className="form-control"
                                id="exampleInputEmail1"
                                type="email"
                                placeholder="Enter email"
                                name="email"
                                value={inputData.email}
                                onChange={this.handleChange}
                              />
                              <small
                                id="emailHelp"
                                class="form-text text-muted"
                              >
                                We'll never share your email with anyone else.
                              </small>
                            </div>
                            <div class="form-group mb-3">
                              <label for="Password">Password</label>
                              <input
                                className="form-control"
                                type="password"
                                id="Password"
                                placeholder="Enter Password"
                                name="password"
                                value={inputData.password}
                                onChange={this.handleChange}
                              />
                            </div>
                            <div class="col-md-12 text-center">
                              <button
                                className="btn btn-lg btn-primary btn-block"
                                disabled={this.props.inputData.buttonDisable}
                              >
                                {this.props.inputData.loadingButtonContent !=
                                null
                                  ? this.props.inputData.loadingButtonContent
                                  : "Submit"}
                              </button>
                            </div>
                          </form>
                          <div class="row mt-3">
                            <div class="col-md-6 col-xs-12 col-sm-12 align-center">
                              <Link className="text-muted" to={"/signup"}>
                                <u>Don't have an Account?</u>
                              </Link>
                            </div>
                            <div class="col-md-6 col-xs-12 col-sm-12 align-center">
                              <Link
                                className="text-muted float-right"
                                to={"/forgot-password"}
                              >
                                <u>Forgot Password?</u>
                              </Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToPros = (state) => ({
  inputData: state.users,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(mapStateToPros, mapDispatchToProps)(LoginIndex);
