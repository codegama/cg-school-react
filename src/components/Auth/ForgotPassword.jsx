import React, { Component } from "react";
import { connect } from "react-redux";
import { forgotPasswordStart } from "../../store/actions/UserAction";
import { Link } from "react-router-dom";

class ForgotPassword extends Component {
  state = {
    inputData: {},
  };

  handleChange = ({ currentTarget: input }) => {
    const inputData = { ...this.state.inputData };
    inputData[input.name] = input.value;
    this.setState({ inputData });
  };

  handleForgotPasswordSubmit = (event) => {
    event.preventDefault();
    this.props.dispatch(forgotPasswordStart(this.state.inputData));
  };
  render() {
    const { inputData } = this.state;
    return (
      <div class="app-admin-wrap layout-sidebar-large">
        <div class="login-box">
          <div class="main-content">
            <div class="row">
              <div class="col-lg-8 col-md-9 col-sm-12 col-xs-12 mx-auto">
                <div class="auth-content">
                  <div class="card o-hidden">
                    <div class="row">
                      <div
                        class="col-md-6 text-center login-img"
                        style={{
                          backgroundSize: "cover",
                          backgroundImage: `url(${window.location.origin}/assets/images/login.jpg)`,
                        }}
                      ></div>
                      <div class="col-md-6">
                        <div class="p-4">
                          <div class="card-title mb-3 text-center">
                            Forgot Password
                          </div>
                          <p class="text-muted text-center">
                            CodeGama - Code of Conduct is Guaranteed | Tech
                            Coder
                          </p>
                          <form onSubmit={this.handleForgotPasswordSubmit}>
                            <div class="form-group mb-3">
                              <label for="exampleInputEmail1">
                                Email address
                              </label>
                              <input
                                className="form-control"
                                id="exampleInputEmail1"
                                type="email"
                                placeholder="Enter email"
                                name="email"
                                value={inputData.email}
                                onChange={this.handleChange}
                              />
                              <small
                                id="emailHelp"
                                class="form-text text-muted"
                              >
                                We'll never share your email with anyone else.
                              </small>
                            </div>
                            <div class="col-md-12 text-center">
                              <button
                                className="btn btn-lg btn-block btn-primary"
                                type="submit"
                                disabled={this.props.inputData.buttonDisable}
                              >
                                {this.props.inputData.loadingButtonContent !=
                                null
                                  ? this.props.inputData.loadingButtonContent
                                  : "Submit"}
                              </button>
                            </div>
                          </form>
                          <div class="row mt-3">
                            <div class="col-md-12 col-xs-12 col-sm-12 text-center">
                              <Link className="text-muted" to={"/login"}>
                                <u>Already have an Account?</u>
                              </Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToPros = (state) => ({
  inputData: state.users,
});

function mapDispatchToProps(dispatch) {
  return { dispatch };
}

export default connect(mapStateToPros, mapDispatchToProps)(ForgotPassword);
