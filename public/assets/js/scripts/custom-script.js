/*------------- Navbar Fixed -------------*/

$(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 50) {
        $(".navbar.navbar-expand-lg").addClass("header-bg");
    } else {
        $(".navbar.navbar-expand-lg").removeClass("header-bg");
    }
});

// testimonial slick slider
$(".testimonial-list").slick({
    dots: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: false,
    arrows:true,
    autoplaySpeed: 5000,
    responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows:false
      }
    }
  ]
});
